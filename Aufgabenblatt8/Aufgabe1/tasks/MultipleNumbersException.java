public class MultipleNumbersException extends Exception {
    public MultipleNumbersException(String message) {
        super(message);
    }
}
