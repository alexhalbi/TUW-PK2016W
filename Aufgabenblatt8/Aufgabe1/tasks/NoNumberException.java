public class NoNumberException extends Exception {
    public NoNumberException(String message) {
        super(message);
    }
}
