
import java.io.*;

/*
    Aufgabe 2) Eingabe und Ausgabe mit Dateien

    Es soll ein Textfile "Geschichte.txt" geöffnet und eingelesen werden.
    Durchsuchen Sie das File nach sogenannten Palindromen (z.b. Hannah, Otto,
    etc.), also Wörtern die von vorn und von hinten gelesen dasselbe ergeben.
    Implementieren Sie dazu eine statische Methode "searchPalindrome(...)", die
    als Argumente die Filenamen für die Ein- und Ausgabe übergeben bekommt.
    Schreiben Sie alle gefundenen Palindrome in die Datei "Palindrome.txt".
    Zusätzlich geben Sie alle Palindrome mit System.out.println(...) aus (in der
    Reihenfolge in der diese im File auftreten).

    Erstellen Sie zusätzlich (unabhängig von Palindromen) eine Statistik über
    alle vorkommenden Buchstaben (auch Umlaute bzw. ein scharfes ß sollen, falls
    vorhanden, aufgelistet werden).
    Groß- und Kleinbuchstaben werden nicht unterschieden. Implementieren Sie
    dazu eine statische Methode "generateStatistic(...)" und geben Sie die
    Ergebnisse in folgender Form aus:
    Buchstabe - Gesamt (Wie oft kommt Buchstabe vor)
    z.B.:   a - 17
            b - 3
            c - 2
            ...

    Zusatzfragen:
    1. Was ist der Unterschied zwischen ungepufferten und gepufferten
       Datenströmen?
       - gepufferter Datenstrom kann ganze Zeile lesen ungepufferter nur nächstes Byte
    2. Was ist der Unterschied zwischen FileInputStream und FileReader?
       - Input Stream ist für Rohdaten wie Bilder gedacht Reader für Text
*/
public class Aufgabe2 {
    
    public static void searchPalindrome(String inFileName, String outFileName){
        try {
            BufferedReader br = new BufferedReader(new FileReader(inFileName));
            BufferedWriter bw = new BufferedWriter(new FileWriter(outFileName));
            String s;
            while((s = br.readLine()) != null) {
                String[] strings = s.replaceAll("[^A-Za-z ]+","").split(" ");
                for (String a:strings) {
                    if(isPalindrome(a)) {
                        System.out.println(a);
                        bw.write(a+"\n");
                    }
                }
            }
            br.close();
            bw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static boolean isPalindrome(String s) {
        s = s.toLowerCase();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != s.charAt(s.length() - 1 - i)) {
                return false;
            }
        }
        return true;
    }
    
    public static void generateStatistic(String inFileName){
        int[] chars = new int[30];
        try {
            FileReader fr = new FileReader(inFileName);
            int i;
            while((i = fr.read())!=-1) {
                if(i<123 && i>96) {
                    chars[i-97] = chars[i-97]+1;
                } else if(i<91 && i>64) {
                    chars[i-65] = chars[i-65]+1;
                } else if(i==228) {
                    chars[26] = chars[26]+1;
                } else if(i==246) {
                    chars[27] = chars[27]+1;
                } else if(i==252) {
                    chars[28] = chars[28]+1;
                } else if(i==223) {
                    chars[29] = chars[29]+1;
                }
            }
            fr.close();
            for(int ii=0;ii<26;ii++) {
                if(chars[ii]>0) System.out.println((char) (ii+97) + " - " + chars[ii]);
            }
            if(chars[26]>0) System.out.println("ä - " + chars[26]);
            if(chars[27]>0) System.out.println("ö - " + chars[27]);
            if(chars[28]>0) System.out.println("ü - " + chars[28]);
            if(chars[29]>0) System.out.println("ß - " + chars[29]);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public static void main(String[] args) {
        System.out.println((int) 'a'); //97-122
        System.out.println((int) 'z'); //97-122
        System.out.println((int) 'A'); //65-90
        System.out.println((int) 'Z'); //65-90
        System.out.println((int) 'ä'); //228
        System.out.println((int) 'ö'); //246
        System.out.println((int) 'ü'); //252
        System.out.println((int) 'ß'); //223
    }
}

