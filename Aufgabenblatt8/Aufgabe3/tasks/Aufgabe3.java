import java.util.HashMap;

/*
    Aufgabe 3) hashCode, equals, Memoization -- Vier Gewinnt

    Fortsetzung des "Vier gewinnt" Spiels aus Aufgabenblatt 7.
    
    Hinweis: Sie können Hilfsmethoden implementieren.
    
    Bei der Berechnung von negamax() wird die selbe Stellung (Spielfeld mit
    bestimmten gesetzten Steinen) mehrmals bewertet: Z.B. ergeben die
    Zugfolgen x3-o2-x4 und x4-o2-x3 die selbe Stellung.  Und die Bewertung
    einer Stellung ist für die gleiche Tiefe auch jedes mal gleich (bzw. das
    Vorzeichen hängt noch vom Spieler ab, aber für die gleiche Stellung ist auch
    immer der gleiche Spieler dran). Da die Bewertung einer Stellung mit
    negamax() relativ teuer ist, vor allem für größere Tiefen, kann man Zeit
    sparen, indem man sich fuer eine berechnete Stellung die Bewertung merkt,
    und wenn sie noch einmal vorkommt, einfach gleich die gespeicherte Bewertung
    zurückgibt, statt sie noch einmal zu berechnen (diese Technik heißt
    Memoization).
    
    Kopieren Sie die Klassen Spielfeld und Viergewinnt, und ändern sie die
    Methode negamax() so, dass sie ab einer gewissen Tiefe vor der Berechnung
    der Bewertung überprüfen, ob die Stellung schon in einer
    HashMap<Spielfeld,Integer> abgespeichert ist, und wenn nicht, nach der
    Berechnung die Stellung und ihre Bewertung in der HashMap eintragen.
    
    Dazu müssen Sie für die Klasse Spielfeld auch die Methoden hashCode() und
    equals() implementieren.  Eine geeignete Hash-Funktion für unser Spielfeld
    ist
    
    (int)((c1*l1+c2*l2)>>32)
    
    wobei l1 und l2 die beiden longs des Spielfeldes sind, und c1 und c2
    grosse ungerade Konstanten, z.B. c1=0x97e2a1430e3ab551L,
    c2=0xddd7aaa5a1ccca9bL.
    
    Weiters dürfen Sie den Key, also das übergebene Spielfeld nach dem Einfügen
    in die HashMap nicht mehr verändern, und müssen daher Ihr Programm deswegen
    eventuell umorganisieren.
    
    Initialisieren Sie (bzw. löschen Sie mit clear() die HashMap an der
    richtigen Stelle, damit Sie einerseits nicht mit alten Bewertungen für
    andere Tiefen arbeiten, andererseits nicht aktuelle Bewertungen wegwerfen.
    
    Experimentieren Sie mit verschiedenen Tiefen, ab der die HashMap verwendet
    wird, und wählen Sie die, die die besten Ergebnisse bringt. Was waren die
    Ergebnisse bei den anderen Tiefen, die Sie ausprobiert haben?

    - Memoization Tiefe>1
		Tiefe=0 Dauer=0 Millis
		Tiefe=1 Dauer=0 Millis
		Tiefe=2 Dauer=0 Millis
		Tiefe=3 Dauer=1 Millis
		Tiefe=4 Dauer=2 Millis
		Tiefe=5 Dauer=2 Millis
		Tiefe=6 Dauer=5 Millis
		Tiefe=7 Dauer=19 Millis
		Tiefe=8 Dauer=48 Millis
		Tiefe=9 Dauer=119 Millis
		Tiefe=10 Dauer=309 Millis
		Tiefe=11 Dauer=861 Millis
		Tiefe=12 Dauer=2380 Millis
		Tiefe=13 Dauer=7917 Millis

	- Memoization Tiefe>2
		Tiefe=0 Dauer=1 Millis
		Tiefe=1 Dauer=0 Millis
		Tiefe=2 Dauer=0 Millis
		Tiefe=3 Dauer=1 Millis
		Tiefe=4 Dauer=2 Millis
		Tiefe=5 Dauer=3 Millis
		Tiefe=6 Dauer=6 Millis
		Tiefe=7 Dauer=22 Millis
		Tiefe=8 Dauer=81 Millis
		Tiefe=9 Dauer=177 Millis
		Tiefe=10 Dauer=487 Millis
		Tiefe=11 Dauer=1237 Millis
		Tiefe=12 Dauer=3402 Millis
		Tiefe=13 Dauer=7708 Millis
		Tiefe=14 Dauer=20388 Millis

	- Memoization Tiefe>3
		Tiefe=0 Dauer=0 Millis
		Tiefe=1 Dauer=0 Millis
		Tiefe=2 Dauer=0 Millis
		Tiefe=3 Dauer=1 Millis
		Tiefe=4 Dauer=3 Millis
		Tiefe=5 Dauer=3 Millis
		Tiefe=6 Dauer=6 Millis
		Tiefe=7 Dauer=38 Millis
		Tiefe=8 Dauer=92 Millis
		Tiefe=9 Dauer=326 Millis
		Tiefe=10 Dauer=797 Millis
		Tiefe=11 Dauer=2523 Millis
		Tiefe=12 Dauer=5793 Millis
		Tiefe=13 Dauer=15497 Millis

	- Memoization Tiefe>4
		Tiefe=0 Dauer=0 Millis
		Tiefe=1 Dauer=0 Millis
		Tiefe=2 Dauer=1 Millis
		Tiefe=3 Dauer=1 Millis
		Tiefe=4 Dauer=1 Millis
		Tiefe=5 Dauer=3 Millis
		Tiefe=6 Dauer=10 Millis
		Tiefe=7 Dauer=35 Millis
		Tiefe=8 Dauer=165 Millis
		Tiefe=9 Dauer=437 Millis
		Tiefe=10 Dauer=1680 Millis
		Tiefe=11 Dauer=4348 Millis
		Tiefe=12 Dauer=13640 Millis

	- Memoization Tiefe>5
		Tiefe=0 Dauer=1 Millis
		Tiefe=1 Dauer=0 Millis
		Tiefe=2 Dauer=0 Millis
		Tiefe=3 Dauer=1 Millis
		Tiefe=4 Dauer=2 Millis
		Tiefe=5 Dauer=4 Millis
		Tiefe=6 Dauer=11 Millis
		Tiefe=7 Dauer=75 Millis
		Tiefe=8 Dauer=159 Millis
		Tiefe=9 Dauer=781 Millis
		Tiefe=10 Dauer=2480 Millis
		Tiefe=11 Dauer=9448 Millis
		Tiefe=12 Dauer=24149 Millis

	- Memoization ohne Minimaltiefe
		Tiefe=0 Dauer=0 Millis
		Tiefe=1 Dauer=0 Millis
		Tiefe=2 Dauer=1 Millis
		Tiefe=3 Dauer=1 Millis
		Tiefe=4 Dauer=2 Millis
		Tiefe=5 Dauer=2 Millis
		Tiefe=6 Dauer=5 Millis
		Tiefe=7 Dauer=17 Millis
		Tiefe=8 Dauer=45 Millis
		Tiefe=9 Dauer=121 Millis
		Tiefe=10 Dauer=314 Millis
		Tiefe=11 Dauer=939 Millis
		Tiefe=12 Dauer=3617 Millis
		Danach >30 sec

    - Long:
        Tiefe=0 Dauer=1 Millis
        Tiefe=1 Dauer=0 Millis
        Tiefe=2 Dauer=0 Millis
        Tiefe=3 Dauer=2 Millis
        Tiefe=4 Dauer=1 Millis
        Tiefe=5 Dauer=4 Millis
        Tiefe=6 Dauer=12 Millis
        Tiefe=7 Dauer=58 Millis
        Tiefe=8 Dauer=374 Millis
        Tiefe=9 Dauer=1869 Millis
        Tiefe=10 Dauer=12302 Millis

     - Klassen:
        Tiefe=0 Dauer=2 Millis
        Tiefe=1 Dauer=1 Millis
        Tiefe=2 Dauer=11 Millis
        Tiefe=3 Dauer=5 Millis
        Tiefe=4 Dauer=48 Millis
        Tiefe=5 Dauer=84 Millis
        Tiefe=6 Dauer=424 Millis
        Tiefe=7 Dauer=3021 Millis
        Tiefe=8 Dauer=19777 Millis

     - Statische Methoden:
        Tiefe=0 Dauer=2 Millis
        Tiefe=1 Dauer=2 Millis
        Tiefe=2 Dauer=10 Millis
        Tiefe=3 Dauer=6 Millis
        Tiefe=4 Dauer=10 Millis
        Tiefe=5 Dauer=54 Millis
        Tiefe=6 Dauer=301 Millis
        Tiefe=7 Dauer=1954 Millis
        Tiefe=8 Dauer=14151 Millis
    
    Zusatzfragen:
    
    1. Geben Sie eine Abschätzung der möglichen Zugfolgen und der möglichen
    Stellungen für Vier Gewinnt an.  Sie müssen dabei nicht ganz genau sein,
    aber die Berechnung sollte schon einigermassen plausibel sein. Welche Fehler
    hat Ihre Berechnung noch?
    
    2. Welche Änderungen waren an Ihrem Programm nötig, um die Anforderungen
    Map() im allgemeinen und von HashMap() im Besonderen zu erfüllen?
    - Hinzufügen einer hash und equals Methode.
        Sonst keine da ich das Spielfeld immer kopiert habe um Züge zu testen.
*/
class Spielfeld {
    private long spieler1;
    private long spieler2;

    public static final int breite = 7;
    public static final int hoehe  = 6;

    public static long getBit(long ID ,int position)
    {
        return (ID >> position) & 1;
    }

    public Spielfeld() {
        spieler1=0;
        spieler2=0;
    }

    private Spielfeld(long spieler1, long spieler2) {
        this.spieler1=spieler1;
        this.spieler2=spieler2;
    }

    public void spielstand(){
        for(int a = hoehe-1; a >= 0; a--) {
            System.out.print('|');
            for (int b = 0; b < breite; b++) {
                switch(feld(a,b)){
                    case 0:
                        System.out.print(' ');
                        break;
                    case 1:
                        System.out.print('x');
                        break;
                    case 2:
                        System.out.print('o');
                        break;
                }
            }
            System.out.print('|'+System.lineSeparator());
        }
        System.out.print('+');
        for(int a = 0; breite > a; a++)
            System.out.print('-');
        System.out.print('+'+System.lineSeparator());
    }

    public int feld(int reihe, int spalte) {
        return (int) (getBit(spieler1,reihe*8+spalte)*1+getBit(spieler2,reihe*8+spalte)*2);
    }

    public Spielfeld copy() { //musste geändert werden da nicht mehr array übergeben wird. vermeidbar, wenn zug rückgängig statt spielfeld klonen. Schnellere ausführung da Array nicht kopiert werden muss.
        return new Spielfeld(spieler1,spieler2);
    }

    public Spielfeld zug(int spieler, int spalte) {
        if(spalte>=breite) return null;
        for (int a = 0; a < hoehe; a++) {
            if (feld(a,spalte) == 0) {
                if(spieler==1) {
                    spieler1 = spieler1 | 1L << a*8+spalte;
                } else {
                    spieler2 = spieler2 | 1L << a*8+spalte;
                }
                return this;
            }
        }
        return null;
    }

    public boolean sieg(int spieler) {
        long x;
        if(spieler == 1) {
            x = spieler1;
        } else {
            x = spieler2;
        }

        return ((x>>(0*9)) & (x>>(1*9)) & (x>>(2*9)) & (x>>(3*9)))>0 ||
                ((x>>(0*1)) & (x>>(1*1)) & (x>>(2*1)) & (x>>(3*1)))>0 ||
                ((x>>(0*7)) & (x>>(1*7)) & (x>>(2*7)) & (x>>(3*7)))>0 ||
                ((x>>(0*8)) & (x>>(1*8)) & (x>>(2*8)) & (x>>(3*8)))>0;
    }

    public int wert1(int spieler){
        int z;
        int d;
        int v;
        long x;
        if(spieler == 1) {
            x = spieler1;
        } else {
            x = spieler2;
        }

        v = Long.bitCount(((x>>(0*9)) & (x>>(1*9)) & (x>>(2*9)) & (x>>(3*9))))+
                Long.bitCount(((x>>(0*1)) & (x>>(1*1)) & (x>>(2*1)) & (x>>(3*1)))) +
                Long.bitCount(((x>>(0*7)) & (x>>(1*7)) & (x>>(2*7)) & (x>>(3*7)))) +
                Long.bitCount(((x>>(0*8)) & (x>>(1*8)) & (x>>(2*8)) & (x>>(3*8))));
        d = Long.bitCount(((x>>(0*9)) & (x>>(1*9)) & (x>>(2*9))))+
                Long.bitCount(((x>>(0*1)) & (x>>(1*1)) & (x>>(2*1)))) +
                Long.bitCount(((x>>(0*7)) & (x>>(1*7)) & (x>>(2*7)))) +
                Long.bitCount(((x>>(0*8)) & (x>>(1*8)) & (x>>(2*8))));
        z = Long.bitCount(((x>>(0*9)) & (x>>(1*9))))+
                Long.bitCount(((x>>(0*1)) & (x>>(1*1)))) +
                Long.bitCount(((x>>(0*7)) & (x>>(1*7)))) +
                Long.bitCount(((x>>(0*8)) & (x>>(1*8))));


        return 1*z+100*d+10000*v;
    }

    public int wert(int spieler){
        switch (spieler) {
            case 1:
                return wert1(1)-wert1(2);
            case 2:
                return wert1(2)-wert1(1);
        }
        return -1;
    }

    public int negamax(int spieler, int tiefe, HashMap<Spielfeld,Integer> NegaMaxErg){
        final int maxTiefe = 1;
        if(tiefe < 1)
            return wert(spieler);
        Spielfeld sim = this.copy();
        int max = Integer.MIN_VALUE;
        if(tiefe>maxTiefe) {
            Integer m;
            if ((m = NegaMaxErg.get(sim)) != null) {
                return m.intValue();
            }
        }
        int nextSpieler = (spieler==1)?2:1;
        for(int s=0, wert;s<=sim.breite;s++) {
            if(sim.zug(spieler,s)!=null) {
                if(sim.sieg(spieler)) {
                    max = Integer.MAX_VALUE;
                    break;
                }
                else
                    wert=-sim.negamax(nextSpieler,tiefe-1, NegaMaxErg);
                if(max<wert)
                    max=wert;
            }
            sim=this.copy();
        }
        if(tiefe>maxTiefe)
            NegaMaxErg.put(this.copy(),max);
        return max;
    }

    public int bester(int spieler, int tiefe){
        Spielfeld sim = this.copy();
        int spalte = -1;
        int max = Integer.MIN_VALUE;
        HashMap<Spielfeld,Integer> NegaMaxErg = new HashMap<>();

        for(int s=0, wert;s<sim.breite;s++) {
            if(sim.zug(spieler,s)!=null) {
                wert=sim.negamax(spieler,tiefe-1,NegaMaxErg);
                if(max<wert) {
                    max=wert;
                    spalte=s;
                }
            }
            sim=this.copy();
        }

        return spalte;
    }

    @Override
    public int hashCode() {
        final long c1=0x97e2a1430e3ab551L;
        final long c2=0xddd7aaa5a1ccca9bL;

        return (int)((c1*spieler1+c2*spieler2)>>32);
    }

    @Override
    public boolean equals(Object obj) {
        if(obj==null) return false;
        if(obj.getClass()==getClass()) {
            Spielfeld s = (Spielfeld) obj;
            return spieler1 == s.spieler1 && spieler2 == s.spieler2;
        }
        return false;
    }
}

class Viergewinnt {
    public static void spiel1(int tiefe){
        Spielfeld f = new Spielfeld();
        int runde = 0;
        int cpu = -1;
        do {
            System.out.print("Spieler beginnt? [Y|N]");
            String s = System.console().readLine();
            if (s.toUpperCase().equals("Y"))
                cpu = 2;
            else if (s.toUpperCase().equals("N"))
                cpu = 1;
            System.out.flush();
        } while (cpu==-1);
        do {
            System.out.flush();
            System.out.println("Runde "+ runde);
            f.spielstand();
            System.out.println("Spieler "+(runde%2 + 1)+" ist am Zug");
            String s = "";
            if((runde%2 + 1)!=cpu) {
                System.out.print("Bitte Reihe angeben [0-" + (f.breite - 1) + "]: ");
                s = System.console().readLine();
            } else {
                s = ""+f.bester(cpu,tiefe);
            }
            try {
                if (f.zug((runde % 2 + 1), Integer.parseInt(s)) == null)
                    continue;
            } catch (Exception ex) {
                continue;
            }
            if(f.sieg((runde%2 + 1))) {
                System.out.flush();
                f.spielstand();
                System.out.println("Spieler "+ (runde%2 + 1) + " gewinnt!");
                break;
            }
            runde++;
        } while(runde <= (f.hoehe * f.breite));
        if(runde > (f.hoehe * f.breite)) {
            System.out.flush();
            f.spielstand();
            System.out.println("Unentschieden");
        }
        System.out.println("Beliebige Taste zum Beenden drücken.");
        String s = System.console().readLine();
    }

    public static void spiel(){
        Spielfeld f = new Spielfeld();
        int runde = 0;
        do {
            System.out.flush();
            System.out.println("Runde "+ runde);
            f.spielstand();
            System.out.println("Spieler "+(runde%2 + 1)+" ist am Zug");
            System.out.print("Bitte Reihe angeben [0-"+(f.breite-1)+"]: ");
            String s = System.console().readLine();
            try{
                if(f.zug((runde%2 + 1),Integer.parseInt(s)) == null)
                    continue;
            } catch (Exception ex) {
                continue;
            }
            if(f.sieg((runde%2 + 1))) {
                System.out.flush();
                f.spielstand();
                System.out.println("Spieler "+ (runde%2 + 1) + " gewinnt!");
                break;
            }
            runde++;
        } while(runde <= (f.hoehe * f.breite));
        if(runde > (f.hoehe * f.breite)) {
            System.out.flush();
            f.spielstand();
            System.out.println("Unentschieden");
        }
        System.out.println("Beliebige Taste zum Beenden drücken.");
        String s = System.console().readLine();
    }
}

public class Aufgabe3 {
    //> java -cp D:\Documents\TUW-PK2016W\Aufgabenblatt8\Aufgabe3\build\classes\main Aufgabe3
    public static void main(String[] args) {

        Viergewinnt.spiel1(11);

        //Viergewinnt.spiel();

        // Test Runtime:
        /*Spielfeld f = new Spielfeld();
        f = f.zug(2,2);
        f = f.zug(1,3);
        f = f.zug(2,4);
        f = f.zug(1,5);
        f = f.zug(1,6);
        f = f.zug(1,2);
        f = f.zug(2,3);
        f = f.zug(2,2);

        for(int i = 0; i<=15;i++) {
            long start = System.currentTimeMillis();
            f.bester(1,i);
            long end = System.currentTimeMillis();
            System.out.println("Tiefe="+i+" Dauer="+(end-start)+" Millis");
        }*/
    }
}

