/*
    Aufgabe 2) Rekursion und Termination

    Die Methoden f, g, h, p und q sind vorgegeben. Rufen Sie in main jede
    dieser Methoden mit allen Argumenten im Bereich von [-10, 10] (in
    aufsteigender Reihenfolge) auf und geben sie die Ergebnisse aus, wenn die
    Aufrufe mit diesen Werten terminieren. Aufrufe, die nicht terminieren
    würden, sind auszulassen. Vermeiden Sie Exceptions.

    Zusatzfragen:
    1. Beschreiben Sie überblicksartig, was die Methoden f, g, h, p und q
    berechnen.
        f) Wenn x^2 > 10 ist dann wird 0 ausgegeben
           Sonst wird der Wert von f(x-1) + 1 ausgegeben
        g) Wenn x negativ ist wird 0 ausgegeben
           Wenn x positiv ist terminiert der Algorithmus nie!
        h) Wenn h > 10 ist wird 0 ausgegeben
           Sonst wird h(x^2)+1 ausgegeben
           Wenn x 0, 1, -1 ist terminiert der Algorithmus nicht,
           da die Funktion mit gleichbleibenden Parametern aufgerufen wird
        p) Wenn x == 0 dann 0
           Wenn x ungerade dann p(x/2) +2
           Wenn x gerade dann p(-x-1)+1
        q) Wenn x vielfaches von 3 dann 0
           Ansonst q(x+(x%3)+1) + 1, wobei (x%3)!=0 ist
           Terminiert nur, wenn (x%3) != 2 ist für x >= 0
           Terminiert nur, wenn (x%3) != 1 ist für x <= 0
    2. Nennen Sie für jeden nicht terminierenden Aufruf von f, g, h, p und q im
    Intervall [-10, 10] einen Grund für die Endlosrekursion.
    2. Bedeutet ein StackOverflowError immer, dass eine Endlosrekursion
    vorhanden ist?
        Nein die Berechnung kann auch einfach mehr Aufrufe brauchen als der Stack groß ist.
        Ob eine berechnung terminiert ist ein nicht entscheidbares Problem
*/
public class Aufgabe2{
    
    private static int f(int x) {
        return x * x > 10 ? 0 : f(x - 1) + 1;
    }
    
    private static int g(int x) {
        return x < 0 ? 0 : g(x / 2) + 1;
    }
    
    private static int h(int x) {
        return x > 10 ? 0 : h(x * x) + 1;
    }
    
    private static int p(int x) {
        return x == 0 ? 0 : x % 2 == 1 ? p(x / 2) + 2 : p(- x - 1) + 1;
    }
    
    private static int q(int x) {
        return x % 3 == 0 ? 0 : q(x + x % 3 + 1) + 1;
    }
    
    public static void main(String[] args) {
        System.out.println("Ergebnisse für f:");
        // TODO: Implementieren Sie hier die Ausgabe für Methode f
        System.out.println(f(-10));
        System.out.println(f(-9));
        System.out.println(f(-8));
        System.out.println(f(-7));
        System.out.println(f(-6));
        System.out.println(f(-5));
        System.out.println(f(-4));
        System.out.println(f(-3));
        System.out.println(f(-2));
        System.out.println(f(-1));
        System.out.println(f(0));
        System.out.println(f(1));
        System.out.println(f(2));
        System.out.println(f(3));
        System.out.println(f(4));
        System.out.println(f(5));
        System.out.println(f(6));
        System.out.println(f(7));
        System.out.println(f(8));
        System.out.println(f(9));
        System.out.println(f(10));
        
        System.out.println("Ergebnisse für g:");
        // TODO: Implementieren Sie hier die Ausgabe für Methode g
        System.out.println(g(-10));
        System.out.println(g(-9));
        System.out.println(g(-8));
        System.out.println(g(-7));
        System.out.println(g(-6));
        System.out.println(g(-5));
        System.out.println(g(-4));
        System.out.println(g(-3));
        System.out.println(g(-2));
        System.out.println(g(-1));
        //System.out.println(g(0));  Endlosrekursion, da g(0) aufgerufen wird
        //System.out.println(g(1));  Endlosrekursion, da g(0) aufgerufen wird
        //System.out.println(g(2));  Endlosrekursion, da g(0) aufgerufen wird
        //System.out.println(g(3));  Endlosrekursion, da g(0) aufgerufen wird
        //System.out.println(g(4));  Endlosrekursion, da g(0) aufgerufen wird
        //System.out.println(g(5));  Endlosrekursion, da g(0) aufgerufen wird
        //System.out.println(g(6));  Endlosrekursion, da g(0) aufgerufen wird
        //System.out.println(g(7));  Endlosrekursion, da g(0) aufgerufen wird
        //System.out.println(g(8));  Endlosrekursion, da g(0) aufgerufen wird
        //System.out.println(g(9));  Endlosrekursion, da g(0) aufgerufen wird
        //System.out.println(g(10)); Endlosrekursion, da g(0) aufgerufen wird
        
        System.out.println("Ergebnisse für h:");
        // TODO: Implementieren Sie hier die Ausgabe für Methode h
        System.out.println(h(-10));
        System.out.println(h(-9));
        System.out.println(h(-8));
        System.out.println(h(-7));
        System.out.println(h(-6));
        System.out.println(h(-5));
        System.out.println(h(-4));
        System.out.println(h(-3));
        System.out.println(h(-2));
        //System.out.println(h(-1)); Endlosrekursion, da h(1) aufgerufen wird
        //System.out.println(h(0));  Endlosrekursion, da h(0) aufgerufen wird
        //System.out.println(h(1));  Endlosrekursion, da h(1) aufgerufen wird
        System.out.println(h(2));
        System.out.println(h(3));
        System.out.println(h(4));
        System.out.println(h(5));
        System.out.println(h(6));
        System.out.println(h(7));
        System.out.println(h(8));
        System.out.println(h(9));
        System.out.println(h(10));

        System.out.println("Ergebnisse für p:");
        // TODO: Implementieren Sie hier die Ausgabe für Methode p
        //System.out.println(p(-10)); Endlosrekursion p(9) wird aufgerufen
        //System.out.println(p(-9));  Endlosrekursion p(8) wird aufgerufen
        System.out.println(p(-8));
        //System.out.println(p(-7));  Endlosrekursion p(6) wird aufgerufen
        //System.out.println(p(-6));  Endlosrekursion p(5) wird aufgerufen
        //System.out.println(p(-5));  Endlosrekursion p(4) wird aufgerufen
        System.out.println(p(-4));
        //System.out.println(p(-3));  Endlosrekursion p(2) wird aufgerufen, da -3 % 2 == -1
        System.out.println(p(-2));
        System.out.println(p(-1));
        System.out.println(p(0));
        System.out.println(p(1));
        //System.out.println(p(2));   Endlosrekursion p(-3) wird aufgerufen
        System.out.println(p(3));
        //System.out.println(p(4));   Endlosrekursion p(2) wird aufgerufen
        //System.out.println(p(5));   Endlosrekursion p(-6) wird aufgerufen
        //System.out.println(p(6));   Endlosrekursion p(-7) wird aufgerufen
        System.out.println(p(7));
        //System.out.println(p(8));   Endlosrekursion p(4) wird aufgerufen
        //System.out.println(p(9));   Endlosrekursion p(-10) wird aufgerufen
        //System.out.println(p(10));  Endlosrekursion p(5) wird aufgerufen

        System.out.println("Ergebnisse für q:");
        // TODO: Implementieren Sie hier die Ausgabe für Methode q
        //System.out.println(q(-10)); Endlosrekursion da x%3 = 1 und x < 0
        System.out.println(q(-9));
        System.out.println(q(-8));
        //System.out.println(q(-7));  Endlosrekursion da x%3 = 1 und x < 0
        System.out.println(q(-6));
        System.out.println(q(-5));
        //System.out.println(q(-4));  Endlosrekursion da x%3 = 1 und x < 0
        System.out.println(q(-3));
        System.out.println(q(-2));
        //System.out.println(q(-1));  Endlosrekursion da x%3 = 1 und x < 0
        System.out.println(q(0));
        System.out.println(q(1));
        //System.out.println(q(2));   Endlosrekursion da x%3 = 2 und x > 0
        System.out.println(q(3));
        System.out.println(q(4));
        //System.out.println(q(5));   Endlosrekursion da x%3 = 2 und x > 0
        System.out.println(q(6));
        System.out.println(q(7));
        //System.out.println(q(8));   Endlosrekursion da x%3 = 2 und x > 0
        System.out.println(q(9));
        System.out.println(q(10));
    }
}

