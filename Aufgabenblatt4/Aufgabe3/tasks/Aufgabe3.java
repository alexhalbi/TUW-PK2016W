/*
    Aufgabe 3, 4, 5,) Zweidimensionale Arrays -- Vier Gewinnt

    Beim Spiel "Vier gewinnt" gewinnt der Spieler, der als erstes vier Steine
    in eine Reihe bringt (horizontal, vertikal, oder diagonal). Das Spielfeld
    steht senkrecht und ist 7 Spalten breit und 6 Reihen hoch. Steine können nur
    im untersten Feld einer Spalte platziert werden, das noch nicht von einem
    anderen Stein besetzt ist.

    "Vier gewinnt" wird in mehreren, aufeinander aufbauenden Aufgaben in
    mehreren Aufgabenblättern verwendet, Sie sollten daher diese Aufgaben
    unbedingt lösen. In diesem Aufgabenblatt deckt das Spiel "Vier gewinnt"
    3 Aufgaben ab. Bitte kreuzen Sie diese separat in TUWEL an.
    
    Hinweis: Sie können Hilfsmethoden implementieren, dürfen aber vorgegebene
             Methoden und deren Signaturen nicht verändern.
    
    *****************************  Aufgabe 3  **********************************
    Für Aufgabe 3 schreiben Sie folgende statische Methoden:

    1) public static int[][] spielfeld()
    
    Diese Methode erzeugt ein leeres Vier-Gewinnt-Spielfeld. Das Spielfeld soll
    als zweidimensionales Array von int-Werten dargestellt werden, wobei auf
    ein Feld in Reihe r und Spalte s im Feld f mit f[r][s] zugegriffen werden
    soll. Ein leeres Feld wird mit 0 repraesentiert, ein Stein auf einem Feld
    durch 1 für einen Stein des Spielers 1 bzw. 2 für einen Stein des
    Spielers 2.

    2) public static void spielstand(int[][] f)
    
    Diese Methode gibt den Spielstand f in folgender Form aus:
    
    |       |
    |       |
    |       |   Definition: Die linke unterste Ecke ist als Koordinate [0][0]
    |       |               definiert und stellt den Ausgangspunkt des
    |  xo   |               Spielbrettes dar.
    |  ox   |
    +-------+
    
    wobei für ein leeres Feld ein Leerzeichen ausgegeben wird, für einen Stein
    von Spieler 1 ein x, und für einen Stein von Spieler 2 ein o.
    
    Zusatzfragen:
    1. Welche anderen Möglichkeiten neben der von Ihnen gewählten gibt es, um
    von der Spielernummer auf x bzw. o zu kommen?
    ****************************************************************************
    
    *****************************  Aufgabe 4  **********************************
    Für Aufgabe 4 schreiben Sie folgende statische Methoden:

    1) public static int[][] zug(int[][] f, int spieler, int spalte)

    Diese Methode führt einen Zug des Spielers "spieler" in Spalte
    "spalte" (0-6 für legale Züge) durch und gibt die neue Stellung
    (das Spielfeld nach dem Zug) zurück.  Wenn in Spalte "spalte" kein
    Zug möglich ist (weil die Spalte voll ist oder nicht im erlaubten
    Bereich), soll zug() null zurückgeben.  Das vom Parameter f
    referenzierte Feld darf verändert werden oder unverändert bleiben.

    2) public static boolean sieg(int[][] f, int spieler)

    Diese Methode liefert true, wenn "spieler" vier Steine in einer Reihe hat,
    sonst false.

    Zusatzfragen:
    1. Welche Vor- und Nachteile hat es für dieses Beispiel und in
    Hinblick auf Aufgabe 5, den Parameter f von zug() zu verändern.
    ****************************************************************************

    *****************************  Aufgabe 5  **********************************
    Für Aufgabe 5 schreiben Sie folgende statische Methode:

    1) public static void spiel()

    Diese Methode führt ein Vier-Gewinnt-Spiel zwischen zwei Spielern durch:
    Beginnend mit einem leeren Spielfeld werden abwechselnd Spieler 1 und
    Spieler 2 zur Eingabe eines Spielzuges aufgefordert, der Spielzug
    durchgeführt, und der aktuelle Spielstand ausgegeben, solange bis ein
    Spieler gewonnen hat oder das Spielfeld voll ist. Überlegen Sie sich
    eine sinnvolle Behandlung von ungültigen Eingaben. In "main" wird am Ende
    nur noch die Methode spiel() aufgerufen. Testen Sie spiel() selbst, auch
    den Fall, dass das Spielfeld voll wird, ohne dass ein Spieler gewonnen hat.

    Zusatzfragen:
    1. Was machen Sie bei ungültigen Eingaben?
        Um eine erneute Eingabe fragen
    ****************************************************************************
*/

import com.sun.javaws.exceptions.InvalidArgumentException;

import java.util.Scanner;
public class Aufgabe3 {

    //***************************  Aufgabe 3  **********************************
    public static int[][] spielfeld(){
        // TODO: Implementieren Sie hier die Angabe
        int[][] i =  new int [6][7];
        for(int a = 0; a<i.length;a++) {
            for(int b = 0; b<i[a].length;b++) {
                i[a][b]=0;
            }
        }
        return i;
    }
    
    public static void spielstand(int[][] f){
        for(int a = f.length-1; a >= 0; a--) {
            System.out.print('|');
            for (int b = 0; b < f[a].length; b++) {
                switch(f[a][b]){
                    case 0:
                        System.out.print(' ');
                        break;
                    case 1:
                        System.out.print('x');
                        break;
                    case 2:
                        System.out.print('o');
                        break;
                }
            }
            System.out.print('|'+System.lineSeparator());
        }
        System.out.print('+');
        for(int a = 0; f[0].length > a; a++)
            System.out.print('-');
        System.out.print('+'+System.lineSeparator());
    }
    //**************************************************************************
    
    
    //***************************  Aufgabe 4  **********************************
    public static int[][] zug(int[][] f, int spieler, int spalte){
        try {
            for (int a = 0; a < f.length; a++) {
                if (f[a][spalte] == 0) {
                    f[a][spalte] = spieler;
                    return f;
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {}
        return null;
    }

    /*
    Direction:
           812
           7x3
           654
     */
    public static boolean sieg(int[][] f, int spieler) {
        for(int a = 0; a < f.length; a++) {
            for(int b = 0; b<f[a].length;b++) {
                if(f[a][b]==spieler)
                    if(checkPosition(f, a, b, 0, 1))
                        return true;
            }
        }
        return false;
    }

    public static boolean checkPosition(int[][]f, int r, int s, int direction, int anz) {
        if(anz == 4)
            return true;

        if(direction == 0)
            return  checkPosition(f, r, s, 1, anz) ||
                    checkPosition(f, r, s, 2, anz) ||
                    checkPosition(f, r, s, 3, anz) ||
                    checkPosition(f, r, s, 4, anz)/* ||
                    checkPosition(f, r, s, 5, anz) ||
                    checkPosition(f, r, s, 6, anz) ||
                    checkPosition(f, r, s, 7, anz) ||
                    checkPosition(f, r, s, 8, anz)*/;

        try {
            switch (direction) {
                case 1:
                    if(f[r][s] == f[r+1][s]) {
                        return checkPosition(f, r+1, s, direction, anz+1);
                    }
                    break;
                case 2:
                    if(f[r][s] == f[r+1][s+1]) {
                        return checkPosition(f, r+1, s+1, direction, anz+1);
                    }
                    break;
                case 3:
                    if(f[r][s] == f[r][s+1]) {
                        return checkPosition(f, r, s+1, direction, anz+1);
                    }
                    break;
                case 4:
                    if(f[r][s] == f[r-1][s+1]) {
                        return checkPosition(f, r-1, s+1, direction, anz+1);
                    }
                    break;
                case 5:
                    if(f[r][s] == f[r-1][s]) {
                        return checkPosition(f, r-1, s, direction, anz+1);
                    }
                    break;
                case 6:
                    if(f[r][s] == f[r-1][s-1]) {
                        return checkPosition(f, r-1, s-1, direction, anz+1);
                    }
                    break;
                case 7:
                    if(f[r][s] == f[r][s-1]) {
                        return checkPosition(f, r, s-1, direction, anz+1);
                    }
                    break;
                case 8:
                    if(f[r][s] == f[r+1][s-1]) {
                        return checkPosition(f, r+1, s-1, direction, anz+1);
                    }
                    break;
            }
        } catch (ArrayIndexOutOfBoundsException e) {}
        return false;
    }
    //**************************************************************************
    
    
    //***************************  Aufgabe 5  **********************************
    public static void spiel(){
        // TODO: Implementieren Sie hier die Angabe
        int [][] f = spielfeld();
        int runde = 0;
        boolean e;
        do {
            e = false;
            System.out.flush();
            System.out.println("Runde "+ runde);
            spielstand(f);
            System.out.println("Spieler "+(runde%2 + 1)+" ist am Zug");
            System.out.print("Bitte Reihe angeben [0-"+(f[0].length-1)+"]: ");
            String s = System.console().readLine();
            try{
                int [][] g = zug(f,(runde%2 + 1),Integer.parseInt(s));
                if(g == null)
                    e = true;
                else
                    f=g;
            } catch (Exception ex) {
                e = true;
            }
            if(!e) {
                if(sieg(f,(runde%2 + 1))) {
                    System.out.flush();
                    spielstand(f);
                    System.out.println("Spieler "+ (runde%2 + 1) + " gewinnt!");
                    break;
                }
                runde++;
            }
        } while(e || runde <= (f.length * f[0].length));
        if(runde > (f.length * f[0].length)) {
            System.out.flush();
            spielstand(f);
            System.out.println("Unentschieden");
        }
        System.out.println("Beliebige Taste zum Beenden drücken.");
        String s = System.console().readLine();
    }
    //**************************************************************************
    
    public static void main(String[] args) {
        spiel();
    }
}

