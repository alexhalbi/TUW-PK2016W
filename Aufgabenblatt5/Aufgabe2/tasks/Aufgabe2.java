import java.util.Stack;

/*
    Aufgabe 2) Deque - Klammerung

    Implementieren Sie in Aufgabe2 eine statische Methode check, die einen
    String als Parameter hat und genau dann true zurückgibt, wenn der String
    einen korrekt geklammerten Ausdruck enthält. Ein Ausdruck ist dann korrekt
    geklammert, wenn es zu jeder öffnenden Klammer '(', '[' oder '{' eine
    entsprechende schließende Klammer ')', ']' oder '}' gibt. Zwischen diesen
    Zeichen können beliebige andere Zeichen vorkommen.

    Beispiele für korrekt geklammerte Ausdrücke:
    "", "a*[a+12]", "a+(b)-c", "a+{b+8+(b+c)}/a"
    Beispiele für nicht korrekt geklammerte Ausdrücke:
    "[", "(}", "a)[]", "([)]", "]["

    Verwenden Sie einen Stack zur Überprüfung der Klammerung: Durchlaufen Sie
    die Zeichen im String von vorne nach hinten und legen Sie jede öffnende
    Klammer, die Sie dabei finden, auf den Stack. Wenn Sie auf eine schließende
    Klammer stoßen, nehmen Sie das oberste Element vom Stack; bei korrekter
    Klammerung muss die schließende Klammer mit der Klammer vom Stack
    zusammenpassen. Andere Zeichen werden einfach ignoriert. Bei korrekter
    Klammerung muss der Stack am Ende leer sein.

    Hinweis: Sie können als Stack z.B. ein Objekt des Typs Deque<String>
    verwenden.

    Zusatzfragen:
    1. Was unterscheidet Queue von Map?
    1. Was ist der Unteschied zwischen LIFO und FIFO Prinzip?
        - Last in First out
            arbeitet wie Stack
        - First in First out
            arbeitet in der selben Reihenfolge wie eingabe ab
    2. Wie könnte man diese Aufgabe auch mit einem Array statt einem Stack
       lösen? Welche Nachteile würden sich daraus ergeben?
*/
public class Aufgabe2{
    
    //TODO Implementieren Sie hier die Methode "check"
    
    public static void main(String[] args) {
    }

    public static boolean check1(String s) {
        if(s.length()==0) return true;
        if((count(s,"(")==count(s,")") && count(s,"[")==count(s,"]") && count(s,"{")==count(s,"}"))
            && (s.indexOf("(")<=s.indexOf(")") && s.indexOf("[")<=s.indexOf("]") & s.indexOf("{")<=s.indexOf("}"))) {
            boolean b = true;
            if(count(s,"(") > 0)
                b = b && check1(s.substring(s.indexOf("(")+1,s.lastIndexOf(")")));
            if(count(s,"[") > 0)
                b = b && check1(s.substring(s.indexOf("[")+1,s.lastIndexOf("]")));
            if(count(s,"{") > 0)
                b = b && check1(s.substring(s.indexOf("{")+1,s.lastIndexOf("}")));
            return b ;
        } else
            return false;
    }

    static int count(String s, String c) {
        int i = 0;
        for(String t=s;t.contains(c);) {
            i++;
            t=t.substring(t.indexOf(c)+c.length());
        }
        return i;
    }

    public static boolean check(String s) {
        Stack<String> stack = new Stack<>();
        for(char c:s.toCharArray()) {
            switch(c) {
                case '(':
                case '[':
                case '{':
                    stack.push(c+"");
                    break;
                case ')':
                    if(stack.isEmpty()||stack.pop().toCharArray()[0]!='(') return false;
                    break;
                case ']':
                    if(stack.isEmpty()||stack.pop().toCharArray()[0]!='[') return false;
                    break;
                case '}':
                    if(stack.isEmpty()||stack.pop().toCharArray()[0]!='{') return false;
                    break;
            }
        }
        return stack.isEmpty();
    }
}

