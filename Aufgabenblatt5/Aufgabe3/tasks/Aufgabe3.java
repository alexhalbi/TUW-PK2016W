/*
    Aufgabe 3, 4, 5) Zweidimensionale Arrays -- Vier Gewinnt

    Fortsetzung des "Vier gewinnt" Spiels aus Aufgabenblatt 4. Kopieren Sie
    sich dazu die Methoden spielfeld(), spielstand(), zug(), und sieg() aus
    Aufgabenblatt 4, um die weiteren Funktionalitäten in diesem Aufgabenblatt
    zu lösen.

    Auch auf diese Aufgaben werden spätere Aufgabenblätter aufbauen, Sie sollten
    sie daher unbedingt lösen. In diesem Aufgabenblatt deckt das Spiel
    "Vier gewinnt" 3 Aufgaben ab. Bitte kreuzen Sie diese separat in TUWEL an.
    
    Hinweis: Sie können Hilfsmethoden implementieren.
    
    *****************************  Aufgabe 3  **********************************
    Für Aufgabe 3 schreiben Sie folgende statische Methoden:

    1) public static int wert1(int[][] f, int spieler)
    
    Diese Methode nimmt eine naive* Stellungsbewertung der Position von Spieler
    "spieler" vor: Es zählt die Zweier-Reihen, Dreier-Reihen, und Vierer-Reihen
    aus Steinen des Spielers "spieler". Der zurückgegebene Wert ist
    1*z+100*d+10000*v, wobei z die Zahl der Zweier-Reihen, d die Zahl der
    Dreier-Reihen, und v die Zahl der Vierer-Reihen ist.
    
    *) Eine einigermassen gute Stellungsbewertung würde den Rahmen der
    Übung sprengen.
    
    Für die Stellung
    
    |       |
    |       |
    |       |
    |  o    |
    |  xo   |
    |  oxoxx|
    +-------+
    
    ist die Bewertung für Spieler 1 (x) 1*2=2 und fuer Spieler 2 (o)
    1*3+100*1=103.  Eine Dreier-Reihe zählt also auch noch als
    zwei Zweier-Reihen.

    2) public static int wert(int[][] f, int spieler)
    
    Die Methode bezieht den wert1() des Gegners in die Bewertung mit ein: Vier
    gewinnt ist (wie die meisten Brettspiele) ein Null-Summen-Spiel
    (Spieler 1 gewinnt, wenn Spieler 2 verliert, und umgekehrt), daher
    soll wert() die Differenz von wert1() des Spielers und wert1() des
    Gegners zurückgeben, im obigen Beispiel also -101 für Spieler 1 (oder
    101 für Spieler 2).
    ****************************************************************************
    
    *****************************  Aufgabe 4  **********************************
    Für Aufgabe 4 schreiben Sie folgende statische Methoden:

    1) public static int negamax(int[][] f, int spieler, int tiefe)
    
    Eine bessere Stellungsbewertung kann man aus wert() ableiten, indem
    man einige Halbzüge vorausschaut. Bei einem Halbzug Vorausschau
    bewertet man die Stellung, die sich bei jedem der 7 möglichen Züge
    ergibt, wie folgt: Der Spieler, der am Zug ist, wird den für ihn
    besten Zug auswählen, der Wert der ursprünglichen Stellung ist also
    das Maximum der Werte der sieben möglichen Folgestellungen. Wenn man
    das für mehrere Halbzüge verallgemeinert, muss man nach jedem Halbzug
    die Seite wechseln, und für den Spieler, der dann am Zug ist, das
    Maximum berechnen. Um diesen Wert dann als Bewertung fuer den anderen
    (vorherigen) Spieler zu verwenden, muß man ihn negieren. Dieser
    Algorithmus heißt "Negamax".

    Diese rekursive Methode "negamax" führt eine Stellungsbewertung für Spieler
    "spieler" mit "tiefe" Zügen Vorausschau durch. Bei 0 Zügen Vorausschau soll
    die Bewertung wert() verwendet werden.
    
    Für diese Methode benötigen Sie eine Möglichkeit, Züge zu probieren,
    ohne sich darauf festzulegen, entweder indem Sie den alten Wert von f
    erhalten, oder indem Sie nach dem Probieren den Zug wieder
    zurücknehmen.  Wenn nötig, modifizieren Sie existierende Methoden
    und/oder implementieren Sie Hilfsmethoden, um das zu erreichen.

    2) public static int bester(int[][] f, int spieler, int tiefe)

    Diese Methode wählt den besten Zug aus. Alle 7 möglichen Züge
    werden durchprobiert, und die sich dadurch ergebende Stellung f1 wird mit
    Hilfe von negamax() bewertet (beachten Sie, welcher Spieler am Zug
    ist). Der Rückgabewert ist ein Zug mit maximaler Bewertung.
    
    Zusatzfragen:
    1. Was sind die Vor- und Nachteile der von Ihnen gewählten
       Art, nach dem Probieren wieder zum vorherigen Zug zu kommen?
       - Hoher Speicherbedarf wird benötigt
    2. Ermitteln Sie durch Ausprobieren und ungefähre Zeitmessung, wie der
       Zeitaufwand von bester() mit der Tiefe zusammenhängt.
        Tiefe=0 Dauer=2 Millis
        Tiefe=1 Dauer=2 Millis
        Tiefe=2 Dauer=10 Millis
        Tiefe=3 Dauer=6 Millis
        Tiefe=4 Dauer=10 Millis
        Tiefe=5 Dauer=54 Millis
        Tiefe=6 Dauer=301 Millis
        Tiefe=7 Dauer=1954 Millis
        Tiefe=8 Dauer=14151 Millis

        Exponentieller Zeitaufwand!
    3. Wieviele Aufrufe von wert() werden höchstens ausgeführt, wenn man
       bester() mit Tiefe 0, 1, 2, 8 und n aufruft?

       0 -> 1
       1 -> 7
       8 -> 7^8 = 5.764.801
       n -> 7^n
    ****************************************************************************

    *****************************  Aufgabe 5  **********************************
    Für Aufgabe 5 schreiben Sie folgende statische Methode:

    1) public static void spiel1(int tiefe)

    Diese Methode führt ein Vier-Gewinnt-Spiel Spieler gegen Computer durch:
    Zunächst sucht sich der Spieler aus, ob er beginnt oder der Computer. Wenn
    der Computer am Zug ist ist, wählt er den nächsten Zug mit bester(...,
    tiefe) aus und führt ihn durch.  Abgesehen davon macht spiel1() das
    gleiche wie spiel(). Probieren Sie verschiedene Werte für tiefe aus,
    und wählen Sie einen, bei dem der Computer im Normalfall zwischen 0.1s
    und 1s zur Auswahl des besten Zugs braucht. Testen Sie die Methode,
    indem Sie gegen den Computer spielen.
    ****************************************************************************
*/
import java.util.Scanner;
public class Aufgabe3 {
    //*********************  Methoden aus Übung 4 ******************************
    public static int[][] spielfeld(){
        // TODO: Implementieren Sie hier die Angabe
        int[][] i =  new int [6][7];
        for(int a = 0; a<i.length;a++) {
            for(int b = 0; b<i[a].length;b++) {
                i[a][b]=0;
            }
        }
        return i;
    }

    public static void spielstand(int[][] f){
        for(int a = f.length-1; a >= 0; a--) {
            System.out.print('|');
            for (int b = 0; b < f[a].length; b++) {
                switch(f[a][b]){
                    case 0:
                        System.out.print(' ');
                        break;
                    case 1:
                        System.out.print('x');
                        break;
                    case 2:
                        System.out.print('o');
                        break;
                }
            }
            System.out.print('|'+System.lineSeparator());
        }
        System.out.print('+');
        for(int a = 0; f[0].length > a; a++)
            System.out.print('-');
        System.out.print('+'+System.lineSeparator());
    }

    public static int[][] zug(int[][] f, int spieler, int spalte){
        int[][] n = new int[f.length][f[0].length];
        for (int a = 0; a < f.length; a++) {
            for (int b = 0; b < f[a].length; b++) {
                n[a][b]=f[a][b];
            }
        }
        try {
            for (int a = 0; a < n.length; a++) {
                if (n[a][spalte] == 0) {
                    n[a][spalte] = spieler;
                    return n;
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {}
        return null;
    }

    /*
    Direction:
           812
           7x3
           654
     */
    public static boolean sieg(int[][] f, int spieler) {
        for(int a = 0; a < f.length; a++) {
            for(int b = 0; b<f[a].length;b++) {
                if(f[a][b]==spieler)
                    if(checkPosition(f, a, b, 0, 1, 4))
                        return true;
            }
        }
        return false;
    }

    public static boolean checkPosition(int[][]f, int r, int s, int direction, int anz, int anzGesucht) {
        if(anz == anzGesucht)
            return true;

        if(direction == 0)
            return  checkPosition(f, r, s, 1, anz,anzGesucht) ||
                    checkPosition(f, r, s, 2, anz,anzGesucht) ||
                    checkPosition(f, r, s, 3, anz,anzGesucht) ||
                    /*checkPosition(f, r, s, 4, anz,anzGesucht) ||
                    checkPosition(f, r, s, 5, anz,anzGesucht) ||
                    checkPosition(f, r, s, 6, anz,anzGesucht) ||
                    checkPosition(f, r, s, 7, anz,anzGesucht) ||*/
                    checkPosition(f, r, s, 8, anz,anzGesucht);

        try {
            switch (direction) {
                case 1:
                    if(f[r][s] == f[r+1][s]) {
                        return checkPosition(f, r+1, s, direction, anz+1,anzGesucht);
                    }
                    break;
                case 2:
                    if(f[r][s] == f[r+1][s+1]) {
                        return checkPosition(f, r+1, s+1, direction, anz+1,anzGesucht);
                    }
                    break;
                case 3:
                    if(f[r][s] == f[r][s+1]) {
                        return checkPosition(f, r, s+1, direction, anz+1,anzGesucht);
                    }
                    break;
                case 4:
                    if(f[r][s] == f[r-1][s+1]) {
                        return checkPosition(f, r-1, s+1, direction, anz+1,anzGesucht);
                    }
                    break;
                case 5:
                    if(f[r][s] == f[r-1][s]) {
                        return checkPosition(f, r-1, s, direction, anz+1,anzGesucht);
                    }
                    break;
                case 6:
                    if(f[r][s] == f[r-1][s-1]) {
                        return checkPosition(f, r-1, s-1, direction, anz+1,anzGesucht);
                    }
                    break;
                case 7:
                    if(f[r][s] == f[r][s-1]) {
                        return checkPosition(f, r, s-1, direction, anz+1,anzGesucht);
                    }
                    break;
                case 8:
                    if(f[r][s] == f[r+1][s-1]) {
                        return checkPosition(f, r+1, s-1, direction, anz+1,anzGesucht);
                    }
                    break;
            }
        } catch (ArrayIndexOutOfBoundsException e) {}
        return false;
    }

    //***************************  Aufgabe 3  **********************************
    public static int wert1(int[][] f, int spieler){
        // TODO: Implementieren Sie hier die Angabe
        int z = 0;
        int d = 0;
        int v = 0;

        for (int a = 0; a < f.length; a++) {
            for (int b = 0; b < f[a].length; b++) {
                if (f[a][b] == spieler) {
                    //if(a==0 || f[a-1][b]!=spieler) {
                        if(checkPosition(f,a,b,1,1,4))  //dir 1
                            v++;
                        if(checkPosition(f,a,b,1,1,3))  //dir 1
                            d++;
                        if(checkPosition(f,a,b,1,1,2))  //dir 1
                            z++;
                    //}
                    //if(a==0 || b==0  || f[a-1][b-1]!=spieler) {
                        if (checkPosition(f, a, b, 2, 1, 4)) //dir 2
                            v++;
                        if (checkPosition(f, a, b, 2, 1, 3)) //dir 2
                            d++;
                        if (checkPosition(f, a, b, 2, 1, 2)) //dir 2
                            z++;
                    //}
                    //if(b==0 || f[a][b-1]!=spieler) {
                        if (checkPosition(f, a, b, 3, 1, 4)) //dir 3
                            v++;
                        if (checkPosition(f, a, b, 3, 1, 3)) //dir 3
                            d++;
                        if (checkPosition(f, a, b, 3, 1, 2)) //dir 3
                            z++;
                    //}
                    //if(a==0 || b==f[a].length-1 || f[a-1][b+1]!=spieler) {
                        if (checkPosition(f, a, b, 8, 1, 4)) //dir 8
                            v++;
                        if (checkPosition(f, a, b, 8, 1, 3)) //dir 8
                            d++;
                        if (checkPosition(f, a, b, 8, 1, 2)) //dir 8
                            z++;
                    //}
                }
            }
        }

        return 1*z+100*d+10000*v;
    }

    public static int wert(int[][] f, int spieler){
        switch (spieler) {
            case 1:
                return wert1(f,1)-wert1(f,2);
            case 2:
                return wert1(f,2)-wert1(f,1);
        }
        return -1;
    }
    //**************************************************************************
    
    
    //***************************  Aufgabe 4  **********************************
    public static int negamax(int[][] f, int spieler, int tiefe){
        if(tiefe < 1)
            return wert(f, spieler);
        int[][] n = f;
        int max = Integer.MIN_VALUE;
        int nextSpieler = (spieler==1)?2:1;

        for(int s=0, wert;s<n[0].length;s++) {
            int[][] z = zug(n,spieler,s);
            if(z!=null) {
                wert=-negamax(z,nextSpieler,tiefe-1);
                if(max<wert)
                    max=wert;
            }
        }

        return max;
    }
    
    
    public static int bester(int[][] f, int spieler, int tiefe){
        int[][] n = f;
        int spalte = -1;
        int max = Integer.MIN_VALUE;

        for(int s=0, wert;s<n[0].length;s++) {
            int[][] z = zug(n,spieler,s);
            if(z!=null) {
                wert=negamax(z,spieler,tiefe-1);
                if(max<wert) {
                    max=wert;
                    spalte=s;
                }
            }
        }

        return spalte;
    }
    //**************************************************************************
    
    
    //***************************  Aufgabe 5  **********************************
    public static void spiel1(int tiefe){
        int [][] f = spielfeld();
        int runde = 0;
        boolean e;
        int cpu = -1;
        do {
            System.out.print("Spieler beginnt? [Y|N]");
            String s = System.console().readLine();
            if (s.toUpperCase().equals("Y"))
                cpu = 2;
            else if (s.toUpperCase().equals("N"))
                cpu = 1;
            System.out.flush();
        } while (cpu==-1);
        do {
            e = false;
            System.out.flush();
            System.out.println("Runde "+ runde);
            spielstand(f);
            System.out.println("Spieler "+(runde%2 + 1)+" ist am Zug");
            String s = "";
            if((runde%2 + 1)!=cpu) {
                System.out.print("Bitte Reihe angeben [0-" + (f[0].length - 1) + "]: ");
                s = System.console().readLine();
            } else {
                s = ""+bester(f,cpu,tiefe);
            }
            try {
                int[][] g = zug(f, (runde % 2 + 1), Integer.parseInt(s));
                if (g == null)
                    e = true;
                else
                    f = g;
            } catch (Exception ex) {
                e = true;
            }

            if(!e) {
                if(sieg(f,(runde%2 + 1))) {
                    System.out.flush();
                    spielstand(f);
                    System.out.println("Spieler "+ (runde%2 + 1) + " gewinnt!");
                    break;
                }
                runde++;
            }
        } while(e || runde <= (f.length * f[0].length));
        if(runde > (f.length * f[0].length)) {
            System.out.flush();
            spielstand(f);
            System.out.println("Unentschieden");
        }
        System.out.println("Beliebige Taste zum Beenden drücken.");
        String s = System.console().readLine();
    }
    //**************************************************************************

    //> java -cp D:\Documents\TUW-PK2016W\Aufgabenblatt5\Aufgabe3\build\classes\main Aufgabe3
    public static void main(String[] args) {
        spiel1(4);

        // Test Runtime:
        /*int [][] f = spielfeld();
        f[0][2]=2;
        f[0][3]=1;
        f[0][4]=2;
        f[0][5]=1;
        f[0][6]=1;
        f[1][2]=1;
        f[1][3]=2;
        f[2][2]=2;

        for(int i = 0; i<=10;i++) {
            long start = System.currentTimeMillis();
            bester(f,1,i);
            long end = System.currentTimeMillis();
            System.out.println("Tiefe="+i+" Dauer="+(end-start)+" Millis");
        }*/
    }
    
}

