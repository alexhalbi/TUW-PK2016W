/*
    Aufgabe 3) Klassen und Objekte -- Vier Gewinnt

    Fortsetzung des "Vier gewinnt" Spiels aus Aufgabenblatt 5.
     
    Auch auf diese Aufgabe werden spätere Aufgabenblätter aufbauen, Sie sollten
    sie daher unbedingt lösen.
    
    Definieren Sie eine Klasse Spielfeld mit einem Konstruktor, der ein leeres
    Spielfeld erzeugt, und folgenden (nicht-statischen) Methoden:

    - int feld(int reihe, int spalte) gibt 0, 1, oder 2 zurück, je nachdem, ob
      das Feld besetzt ist und von wem.

    Folgende Methoden sind nicht-statische Varianten für Spielfeld der früher
    definierten statischen Methoden:

    - void spielstand()
    - Spielfeld zug(int spieler, int Spalte)
    - boolean sieg(int spieler)
    - int wert1(int spieler)
    - int wert(int spieler)
    - int negamax(int spieler, int tiefe)
    - int bester(int spieler, int tiefe)

    Das Spielfeld wird dabei noch immer so repräsentiert wie zuvor.

    Schreiben Sie eine Klasse Viergewinnt mit den Methoden

    - public static void spiel()
    - public static void spiel1(int tiefe)

    die für das Spielfeld und die Methoden dazu die Klasse Spielfeld benutzen.

    Zusatzfrage: Ist spiel1() bei gleicher Tiefe durch diese Änderung
    schneller oder langsamer geworden? Um wieviel?

     - Klassen:
        Tiefe=0 Dauer=2 Millis
        Tiefe=1 Dauer=1 Millis
        Tiefe=2 Dauer=11 Millis
        Tiefe=3 Dauer=5 Millis
        Tiefe=4 Dauer=48 Millis
        Tiefe=5 Dauer=84 Millis
        Tiefe=6 Dauer=424 Millis
        Tiefe=7 Dauer=3021 Millis
        Tiefe=8 Dauer=19777 Millis

     - Statische Methoden:
        Tiefe=0 Dauer=2 Millis
        Tiefe=1 Dauer=2 Millis
        Tiefe=2 Dauer=10 Millis
        Tiefe=3 Dauer=6 Millis
        Tiefe=4 Dauer=10 Millis
        Tiefe=5 Dauer=54 Millis
        Tiefe=6 Dauer=301 Millis
        Tiefe=7 Dauer=1954 Millis
        Tiefe=8 Dauer=14151 Millis
    ****************************************************************************
*/

class Spielfeld {
    private int[][] feld;

    public static final int breite = 7;
    public static final int hoehe  = 6;

    public Spielfeld() {
        feld =  new int [hoehe][breite];
        for(int a = 0; a<feld.length;a++) {
            for(int b = 0; b<feld[a].length;b++) {
                feld[a][b]=0;
            }
        }
    }

    private Spielfeld(int[][] f) {
        if(f == null) {
            throw new IllegalArgumentException("Array null!");
        }
        if(hoehe != f.length) {
            throw new IllegalArgumentException("Wrong Array length!");
        }
        for(int[]i: f) {
            if(breite != i.length) {
                throw new IllegalArgumentException("Wrong Array length!");
            }
        }

        feld = f;
    }

    public void spielstand(){
        for(int a = hoehe-1; a >= 0; a--) {
            System.out.print('|');
            for (int b = 0; b < breite; b++) {
                switch(feld(a,b)){
                    case 0:
                        System.out.print(' ');
                        break;
                    case 1:
                        System.out.print('x');
                        break;
                    case 2:
                        System.out.print('o');
                        break;
                }
            }
            System.out.print('|'+System.lineSeparator());
        }
        System.out.print('+');
        for(int a = 0; breite > a; a++)
            System.out.print('-');
        System.out.print('+'+System.lineSeparator());
    }

    public int feld(int reihe, int spalte) {
        return feld[reihe][spalte];
    }

    public Spielfeld copy() {
        int[][] f = new int [hoehe][breite];
        for (int a = 0; a < hoehe; a++) {
            for (int b = 0; b < breite; b++) {
                f[a][b]=feld[a][b];
            }
        }
        return new Spielfeld(f);
    }

    public Spielfeld zug(int spieler, int spalte) {
        try {
            for (int a = 0; a < hoehe; a++) {
                if (feld(a,spalte) == 0) {
                    feld[a][spalte] = spieler;
                    return this;
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {}
        return null;
    }

    /*
    Direction:
           812
           7x3
           654
     */
    public boolean sieg(int spieler) {
        for(int a = 0; a < hoehe; a++) {
            for(int b = 0; b< breite;b++) {
                if(feld(a,b)==spieler)
                    if(checkPosition(this, a, b, 0, 1, 4))
                        return true;
            }
        }
        return false;
    }

    public static boolean checkPosition(Spielfeld f, int r, int s, int direction, int anz, int anzGesucht) {
        if(anz == anzGesucht)
            return true;

        if(direction == 0)
            return  checkPosition(f, r, s, 1, anz,anzGesucht) ||
                    checkPosition(f, r, s, 2, anz,anzGesucht) ||
                    checkPosition(f, r, s, 3, anz,anzGesucht) ||
                    /*checkPosition(f, r, s, 4, anz,anzGesucht) ||
                    checkPosition(f, r, s, 5, anz,anzGesucht) ||
                    checkPosition(f, r, s, 6, anz,anzGesucht) ||
                    checkPosition(f, r, s, 7, anz,anzGesucht) ||*/
                    checkPosition(f, r, s, 8, anz,anzGesucht);

        try {
            switch (direction) {
                case 1:
                    if(f.feld(r,s) == f.feld(r+1,s)) {
                        return checkPosition(f, r+1, s, direction, anz+1,anzGesucht);
                    }
                    break;
                case 2:
                    if(f.feld(r,s) == f.feld(r+1,s+1)) {
                        return checkPosition(f, r+1, s+1, direction, anz+1,anzGesucht);
                    }
                    break;
                case 3:
                    if(f.feld(r,s) == f.feld(r,s+1)) {
                        return checkPosition(f, r, s+1, direction, anz+1,anzGesucht);
                    }
                    break;
                case 4:
                    if(f.feld(r,s) == f.feld(r-1,s+1)) {
                        return checkPosition(f, r-1, s+1, direction, anz+1,anzGesucht);
                    }
                    break;
                case 5:
                    if(f.feld(r,s) == f.feld(r-1,s)) {
                        return checkPosition(f, r-1, s, direction, anz+1,anzGesucht);
                    }
                    break;
                case 6:
                    if(f.feld(r,s) == f.feld(r-1,s-1)) {
                        return checkPosition(f, r-1, s-1, direction, anz+1,anzGesucht);
                    }
                    break;
                case 7:
                    if(f.feld(r,s) == f.feld(r,s-1)) {
                        return checkPosition(f, r, s-1, direction, anz+1,anzGesucht);
                    }
                    break;
                case 8:
                    if(f.feld(r,s) == f.feld(r+1,s-1)) {
                        return checkPosition(f, r+1, s-1, direction, anz+1,anzGesucht);
                    }
                    break;
            }
        } catch (ArrayIndexOutOfBoundsException e) {}
        return false;
    }

    public int wert1(int spieler){
        int z = 0;
        int d = 0;
        int v = 0;

        for (int a = 0; a < hoehe; a++) {
            for (int b = 0; b < breite; b++) {
                if (feld(a,b) == spieler) {
                    //if(a==0 || feld.feld(a-1,b)!=spieler) {
                    if(checkPosition(this,a,b,1,1,4))  //dir 1
                        v++;
                    if(checkPosition(this,a,b,1,1,3))  //dir 1
                        d++;
                    if(checkPosition(this,a,b,1,1,2))  //dir 1
                        z++;
                    //}
                    //if(a==0 || b==0  || feld.feld(a-1,b-1!=spieler) {
                    if (checkPosition(this, a, b, 2, 1, 4)) //dir 2
                        v++;
                    if (checkPosition(this, a, b, 2, 1, 3)) //dir 2
                        d++;
                    if (checkPosition(this, a, b, 2, 1, 2)) //dir 2
                        z++;
                    //}
                    //if(b==0 || feld.feld(a,b-1)!=spieler) {
                    if (checkPosition(this, a, b, 3, 1, 4)) //dir 3
                        v++;
                    if (checkPosition(this, a, b, 3, 1, 3)) //dir 3
                        d++;
                    if (checkPosition(this, a, b, 3, 1, 2)) //dir 3
                        z++;
                    //}
                    //if(a==0 || b==feld.breite-1 || feldfeld.feld(a-1,b+1)!=spieler) {
                    if (checkPosition(this, a, b, 8, 1, 4)) //dir 8
                        v++;
                    if (checkPosition(this, a, b, 8, 1, 3)) //dir 8
                        d++;
                    if (checkPosition(this, a, b, 8, 1, 2)) //dir 8
                        z++;
                    //}
                }
            }
        }

        return 1*z+100*d+10000*v;
    }

    public int wert(int spieler){
        switch (spieler) {
            case 1:
                return wert1(1)-wert1(2);
            case 2:
                return wert1(2)-wert1(1);
        }
        return -1;
    }

    public int negamax(int spieler, int tiefe){
        if(tiefe < 1)
            return wert(spieler);
        Spielfeld sim = this.copy();
        int max = Integer.MIN_VALUE;
        int nextSpieler = (spieler==1)?2:1;
        for(int s=0, wert;s<=sim.breite;s++) {
            if(sim.zug(spieler,s)!=null) {
                if(sim.sieg(spieler))
                    wert = Integer.MAX_VALUE;
                else
                    wert=-sim.negamax(nextSpieler,tiefe-1);
                if(max<wert)
                    max=wert;
            }
            sim=this.copy();
        }
        return max;
    }

    public int bester(int spieler, int tiefe){
        Spielfeld sim = this.copy();
        int spalte = -1;
        int max = Integer.MIN_VALUE;

        for(int s=0, wert;s<sim.breite;s++) {
            if(sim.zug(spieler,s)!=null) {
                wert=sim.negamax(spieler,tiefe-1);
                if(max<wert) {
                    max=wert;
                    spalte=s;
                }
            }
            sim=this.copy();
        }

        return spalte;
    }
}

class Viergewinnt {
    public static void spiel1(int tiefe){
        Spielfeld f = new Spielfeld();
        int runde = 0;
        int cpu = -1;
        do {
            System.out.print("Spieler beginnt? [Y|N]");
            String s = System.console().readLine();
            if (s.toUpperCase().equals("Y"))
                cpu = 2;
            else if (s.toUpperCase().equals("N"))
                cpu = 1;
            System.out.flush();
        } while (cpu==-1);
        do {
            System.out.flush();
            System.out.println("Runde "+ runde);
            f.spielstand();
            System.out.println("Spieler "+(runde%2 + 1)+" ist am Zug");
            String s = "";
            if((runde%2 + 1)!=cpu) {
                System.out.print("Bitte Reihe angeben [0-" + (f.breite - 1) + "]: ");
                s = System.console().readLine();
            } else {
                s = ""+f.bester(cpu,tiefe);
            }
            try {
                if (f.zug((runde % 2 + 1), Integer.parseInt(s)) == null)
                    continue;
            } catch (Exception ex) {
                continue;
            }
            if(f.sieg((runde%2 + 1))) {
                System.out.flush();
                f.spielstand();
                System.out.println("Spieler "+ (runde%2 + 1) + " gewinnt!");
                break;
            }
            runde++;
        } while(runde <= (f.hoehe * f.breite));
        if(runde > (f.hoehe * f.breite)) {
            System.out.flush();
            f.spielstand();
            System.out.println("Unentschieden");
        }
        System.out.println("Beliebige Taste zum Beenden drücken.");
        String s = System.console().readLine();
    }

    public static void spiel(){
        Spielfeld f = new Spielfeld();
        int runde = 0;
        do {
            System.out.flush();
            System.out.println("Runde "+ runde);
            f.spielstand();
            System.out.println("Spieler "+(runde%2 + 1)+" ist am Zug");
            System.out.print("Bitte Reihe angeben [0-"+(f.breite-1)+"]: ");
            String s = System.console().readLine();
            try{
                if(f.zug((runde%2 + 1),Integer.parseInt(s)) == null)
                    continue;
            } catch (Exception ex) {
                continue;
            }
            if(f.sieg((runde%2 + 1))) {
                System.out.flush();
                f.spielstand();
                System.out.println("Spieler "+ (runde%2 + 1) + " gewinnt!");
                break;
            }
            runde++;
        } while(runde <= (f.hoehe * f.breite));
        if(runde > (f.hoehe * f.breite)) {
            System.out.flush();
            f.spielstand();
            System.out.println("Unentschieden");
        }
        System.out.println("Beliebige Taste zum Beenden drücken.");
        String s = System.console().readLine();
    }
}
public class Aufgabe3 {

    //> java -cp D:\Documents\TUW-PK2016W\Aufgabenblatt6\Aufgabe3\build\classes\main Aufgabe3
    public static void main(String[] args) {

        Viergewinnt.spiel1(6);

        //Viergewinnt.spiel();

        // Test Runtime:
        /*Spielfeld f = new Spielfeld();
        f = f.zug(2,2);
        f = f.zug(1,3);
        f = f.zug(2,4);
        f = f.zug(1,5);
        f = f.zug(1,6);
        f = f.zug(1,2);
        f = f.zug(2,3);
        f = f.zug(2,2);

        for(int i = 0; i<=10;i++) {
            long start = System.currentTimeMillis();
            f.bester(1,i);
            long end = System.currentTimeMillis();
            System.out.println("Tiefe="+i+" Dauer="+(end-start)+" Millis");
        }*/
    }
}

