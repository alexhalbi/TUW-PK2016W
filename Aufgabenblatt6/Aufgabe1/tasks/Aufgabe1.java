/*******************************************************************************

 AUFGABENBLATT 6 - Allgemeine Informationen

 Achten Sie bei der Implementierung auf folgende Punkte:

 - Ihr Programm sollte den dazugehörenden Test (z.B. enthält Aufgabe1Test den
 Test zu Aufgabe1) bestehen.

 - Bei jeder Aufgabe finden Sie Zusatzfragen. Diese Zusatzfragen beziehen sich
 thematisch auf das erstellte Programm.  Sie müssen diese Zusatzfragen in der
 Übung beantworten können.

 - Verwenden Sie bei allen Ausgaben immer System.out.println().

 - Verwenden Sie für die Lösung der Aufgaben keine speziellen Aufrufe aus der
 Java-API, die die Aufgaben verkürzen würden.

 Abgabe: Die Abgabe erfolgt in TUWEL. Bitte laden Sie Ihr IntelliJ-Projekt
 bis spätestens Montag 12.12.2016 08:00 Uhr in TUWEL hoch. Zusätzlich
 müssen Sie in TUWEL ankreuzen welche Aufgaben Sie gelöst haben und während
 der Übung präsentieren können.

 ******************************************************************************/
/*
    Aufgabe 1) Sortieren & Suchen

    Implementieren Sie in dieser Aufgabe in der gegebenen Klasse Aufgabe1
    folgende statische Methoden:

    - sort:       Diese Methode soll den Sortieralgorihtmus "QuickSort"
                  implementieren. Sie müssen den Sortieralgorithmus selbst
                  ausimplementieren und dürfen keinen entsprechenden Aufruf aus
                  der Java-API verwenden.

    - binSearch:  Dieser Methode wird ein sortiertes Array übergeben.
                  Zusätzlich erhält die Methode einen Wert nach dem gesucht
                  werden soll. Es soll eine binäre Suche implementiert werden,
                  die true zurückliefert falls das Element enthalten ist,
                  ansonsten false.
                  
    Hinweis: Sie dürfen zusätzliche Hilfsmethoden implementieren und verwenden!

    Zusatzfragen:
    1. Welche API-Aufrufe bietet Java für das Sortieren von Arrays an?
        - java.util.Arrays.sort()
        - java.util.Arrays.parallelSort()
    2. Welcher Sortieralgorithmus wird in der Java (1.8) für das Sortieren von
       Arrays verwendet?
        - Dual-Pivot Quicksort by Vladimir Yaroslavskiy, Jon Bentley, and Joshua Bloch
    3. Warum ist die Wahl des Pivot-Elements entscheidend für die Performance
       des Quicksort Algorithmus?
        - Es muss nahe des Medians der Arraywerte sein.
    4. Warum muss das Array für die binäre Suche sortiert sein?
        - Weil sonst die Mitte des Arrays nicht die Mitte der Werte ist!
    5. Wie geht man vor wenn man in einem absteigend sortierten Array die
       Binärsuche anwenden will?
        - Vertauschen des Vergleichsoperators (Zeile 91)
*/
public class Aufgabe1 {

    public static void sort(int[] array) {
        quickSort(array,0,array.length-1);
    }

    public static void quickSort(int[] array, int l, int r) {
        if(l<r) {
            int teiler = split(array,l,r);
            quickSort(array,l,teiler-1);
            quickSort(array,teiler+1,r);
        }
    }

    public static int split(int[]array, int l, int r) {
        int i = l;
        int j = r;
        int pivot = array[r];
        do {
            while(array[i]<=pivot&& i<r) {
                i++;
            }
            while(array[j]>=pivot&&j>l) {
                j--;
            }
            if(i<j) {
                int a = array[i];
                array[i]=array[j];
                array[j]=a;
            }
        } while(i<j);
        if(array[i]>pivot) {
            int a = array[i];
            array[i]=array[r];
            array[r]=a;
        }
        return i;
    }

    public static void bubbleSort(int[] array) {
        boolean g;
        int n = 2;
        for(int i = array.length-1;n>1;i--) {  /*i>1&&g*/
            g = false;
            n = 1;
            for(int j = 0; j<i;j++) {
                if(array[j+1]<array[j]){
                    int a = array[j];
                    array[j] = array[j+1];
                    array[j+1] = a;
                    g = true;
                    n = j+1;
                }
            }
            if(!g)
                return;
            i=n;
        }
    }
    
    
    public static boolean binSearch(int[] array, int elem) {
        int l = 0;
        int r = array.length-1;
        while(l<=r) {
            int m = (r+l)/2;
            if(array[m]==elem){
                return true;
            } else if (array[m]<elem) {
                l = m+1;
            } else {
                r = m-1;
            }
        }
        return false;
    }
    
    
    public static void main(String[] args) {
    }
}



