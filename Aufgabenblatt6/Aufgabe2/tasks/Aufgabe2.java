/*
    Aufgabe 2) Listen

    In der Klasse IntList haben Sie eine Listenimplementierung gegeben.
    Dazu sollen Sie folgende zusätzliche Methoden implementieren:

    - reverseI: (iterativ) Dreht die Liste um. Die Methode muss iterativ
                implementiert werden. Bei dieser Methode werden keine (!) neuen
                Knoten erzeugt und die Werte (elem-Felder) der Knoten dürfen
                nicht überschrieben werden. Die Umkehrung der Liste wird nur
                durch die Neuverkettung der vorhandenen Knoten erreicht!

    - reverseR: (rekursiv) Dreht die Liste um. Die Methode muss rekursiv
                implementiert werden. Die eigentliche Rekursion sollte in der
                Klasse ListNode durchgeführt werden. Bei dieser Methode werden
                keine (!) neuen Knoten erzeugt und die Werte der Knoten dürfen
                nicht überschrieben werden. Die Umkehrung der Liste wird nur
                durch die Neuverkettung der vorhandenen Knoten erreicht!
                
    - insertIdx:(iterativ) Fügt einen Knoten entsprechend dem übergebenen
                Index ein. Das Head-Element hat den Index 1. Hängen Sie an
                entsprechender Stelle die Liste so um, damit diese nach
                Einfügen wieder eine korrekte Verkettung aufweist. Ist der Index
                größer als die Länge der Liste wird false zurückgegeben.
                Bei erfolgreichem Einfügen wird true retourniert.
                
    - remove:   (iterativ) Entfernt aus der Liste alle Knoten mit dem
                übergebenen Wert (elem), falls Knoten mit diesem Element
                vorhanden sind. Bei erfolgreichem Entfernen wird true
                zurückgegeben, ansonsten false.

    Zusatzfragen:
    1. Wie entsteht die Ausgabe beim Aufruf der Methode
       System.out.println(list);
        - Über die toString Methoden der Objekte
    2. Warum ist es sinnvoll beim Iterieren durch Listen sich eine Kopie des
       Zeigers auf den "head"-Knoten zu erstellen?
        - Um schnell zum Anfang zurückkehren zu können.
    3. Erläutern Sie die Vor- und Nachteile von Listen gegenüber Arrays.
        - Listen können sehr schnell Iteriert werden und man kommt schnell von einem Obejkt zum nächsten ohne Zähler
        - Wenn man jedoch den 10 Eintrag braucht dauert dies Vergleichsweise lange da 10 Aufrufe von getNext() geschehen
*/
class IntList {
    
    private class ListNode {
        int elem;
        ListNode next = null;
        
        ListNode(int elem, ListNode next) {
            this.elem = elem;
            this.next = next;
        }
        
        int getElem() {
            return this.elem;
        }
        
        ListNode getNext() {
            return this.next;
        }
        
        void add(int elem) {
            if (this.next == null) {
                this.next = new ListNode(elem, null);
            } else {
                this.next.add(elem);
            }
        }
        
        ListNode reverseR() {
            if(next==null) {
                return this;
            }
            ListNode n = next;
            next = null;

            ListNode rest = n.reverseR();

            n.next = this;
            return rest;
        }
        
        public String toString() {
            return this.elem + ((this.next == null) ? "-|" : "->" + this.next);
        }
        
    }
    
    private ListNode head = null;
    
    public boolean empty() {
        return this.head == null;
    }
    
    public void add(int elem) {
        if (this.empty()) {
            this.head = new ListNode(elem, null);
        } else {
            this.head.add(elem);
        }
    }
    
    public int first() {
        return this.head.getElem();
    }
    
    public void reverseI() {
        if(head==null)
            return;
        ListNode e = head;
        ListNode last = null;
        while(e.getNext()!=null) {
            ListNode next = e.getNext();
            e.next = last;
            last = e;
            e = next;
        }
        e.next = last;
        head = e;
    }
    
    public void reverseR() {
        this.head = this.head.reverseR();
    }
    
    public boolean insertIdx(int value, int index) {
        if(index<1)
            return false;
        else if (index == 1) {
            ListNode v = new ListNode(value, head);
            head = v;
            return true;
        }
        ListNode x = head;
        try {
            for (int i = 1; i < index-1; i++) {
                x = x.getNext();
            }
            ListNode v = new ListNode(value, x.getNext()); //x-> Element vor Index x.next-> Element auf Index
            x.next = v;
            return true;
        } catch (NullPointerException e){
            return false;
        }
    }
    
    public boolean remove(int value) {
        if(head==null)
            return false;
        boolean ret = false;
        while(head.getElem()==value) {
            head = head.getNext();
            ret = true;
            if(head==null)
                return ret;
        }
        ListNode x = head;
        while(x.getNext()!=null) {
            if(x.getNext().getElem()==value) {
                x.next = x.getNext().getNext();
                ret = true;
            } else
                x=x.getNext();
        }
        return ret;
    }
    
    public String toString() {
        return "[" + this.head + "]";
    }
}

public class Aufgabe2{
    
    public static void main(String[] args) {
        // Inhalt von "main" wird für die Unittests verwendet!
        IntList myList = new IntList();
        for(int i = 1; i < 6; i++){
            myList.add(i);
        }
        myList.add(25);
        myList.add(15);
        myList.add(1);
        myList.add(5);
        System.out.println(myList);
        myList.reverseI();
        System.out.println(myList);
        myList.reverseR();
        System.out.println(myList);
        myList.insertIdx(0,1);// 0->1->2->3->4->5->25->15->1->5-|
        myList.insertIdx(2,7);// 0->1->2->3->4->5->2->25->15->1->5-|
        myList.insertIdx(18,11);// 0->1->2->3->4->5->2->25->15->1->11->5-|
        myList.insertIdx(41,20); // nothing
        myList.insertIdx(23,12);// 0->1->2->3->4->5->2->25->15->1->11->23->5-|
        myList.insertIdx(344,0);// nothing
        myList.insertIdx(1,2);// 0->1->1->2->3->4->5->2->25->15->1->11->23->5-|
        myList.insertIdx(5,14);// 0->1->1->2->3->4->5->2->25->15->1->11->23->14->5-|
        myList.insertIdx(4,6);
        System.out.println(myList);
        myList.remove(0);
        myList.remove(1);
        myList.remove(7);
        myList.remove(5);
        myList.remove(25);
        myList.remove(4);
        System.out.println(myList);
    }
}


