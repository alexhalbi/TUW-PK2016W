import java.util.ArrayList;
import java.util.List;

public class Person {
    String vorname;
    String nachname;
    boolean geschlecht;  //männlich=true
    int alter;
    int sozialversicherungsnummer;
    List<Person> kinder;

    public Person(String vorname,String nachname,boolean geschlecht,int alter,int sozialversicherungsnummer) {
        this.vorname=vorname;
        this.nachname=nachname;
        this.geschlecht=geschlecht;
        this.alter=alter;
        this.sozialversicherungsnummer=sozialversicherungsnummer;
        kinder = new ArrayList<>();
    }

    public void neuesKind(Person p) {
        for (Person k:kinder) {
            if(k.equals(p)) {
                return;
            }
        }
        kinder.add(p);
    }

    @Override
    public String toString() {
        String s = "# "+vorname + ' ' + nachname +", " + (geschlecht?"maennlich":"weiblich")+ ", " + alter + " Jahre, Svnr: " + sozialversicherungsnummer;
        if(kinder!=null) {
            for (Person k:kinder) {
                s += "\n "+ k.toString().replace("\n","\n ");
            }
        }
        return s;
    }

    @Override
    public int hashCode() {
        int multiplier = 79;
        int hash = 12;
        hash = hash*multiplier + vorname.hashCode();
        hash = hash*multiplier + nachname.hashCode();
        hash = hash*multiplier + sozialversicherungsnummer;

        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj==null) return false;
        if(obj.getClass() == this.getClass()) {
            Person o = (Person) obj;
            return o.vorname==vorname && o.nachname==nachname && o.sozialversicherungsnummer == sozialversicherungsnummer;
        }
        return false;
    }
}
