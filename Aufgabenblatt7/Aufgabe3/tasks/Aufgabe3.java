/*
    Aufgabe 3) Datenrepräsentation -- Vier Gewinnt

    Fortsetzung des "Vier gewinnt" Spiels aus Aufgabenblatt 6.
     
    Auch auf diese Aufgaben werden spätere Aufgabenblätter aufbauen, Sie sollten
    sie daher unbedingt lösen.
    
    Hinweis: Sie können Hilfsmethoden implementieren.
    
    Kopieren Sie die Klassen Spielfeld und Viergewinnt, und ersetzen Sie die
    Repräsentation des Spielfeldes in der Klasse Spielfeld durch folgende:
    Das Spielfeld wird durch zwei longs repräsentiert: Im ersten long sind die
    Bits gesetzt, die den Steinen des ersten Spielers entsprechen, im zweiten
    die, die den Steinen des zweiten Spielers entsprechen. Die Nummerierung der
    Bits im Spielfeld ist wie folgt:
    
    |40 41 42 43 44 45 46|47
    |32 33 34 35 36 37 38|39
    |24 25 26 27 28 29 30|31
    |16 17 18 19 20 21 22|23
    | 8  9 10 11 12 13 14|15
    | 0  1  2  3  4  5  6| 7
    +--------------------+
    
    Wobei die bits 7, 15, 23, 31, 39, 47 keinen Feldern des Spielfelds
    entsprechen und immer auf 0 bleiben müssen.
    
    Durch diese Repräsentation kann die Überprüfung für sieg() und wert1()
    wesentlich schneller durchgeführt werden: Beachten Sie, dass der rechte
    Nachbar eine um 1 höhere Nummer hat, der Nachbar links oben eine um 7 höhere
    Nummer, der Nachbar oben eine um 8 höhere Nummer, und der Nachbar rechts
    oben eine um 9 höhere Nummer.  Wenn wir also z.B.
    
    (x>>(0*9)) & (x>>(1*9)) & (x>>(2*9)) & (x>>(3*9))
    
    berechnen, und im Resultat z.B. an Stelle 3 ein Bit gesetzt ist, heisst das,
    dass in x an Stelle 3, 12, 21, 30 ein Bit gesetzt ist und damit vier in
    einer Diagonale nach rechts oben gesetzt sind. Allgemeiner, wenn irgendein
    Bit im Resultat gesetzt ist (das Resultat also ungleich 0 ist), hat man
    irgendwo vier in einer Diagonale nach rechts oben; die achte, leere Spalte,
    sorgt dafuer, dass horizontal und diagonal keine Reihen jenseits des Randes
    fortgesetzt werden. Durch Verwenden von shifts um n*1, n*7, und n*8
    überprüfen Sie entsprechend auch horizontale Reihen, Reihen nach links oben,
    und Reihen nach oben.
    
    Für wert1() verwendet man entsprechend Ausdrücke auch mit drei und nur zwei
    Termen. Um dann für die Berechnung des Wertes die Anzahl der Reihen
    (also bits) zu zählen, kann man die Methode java.lang.Long.bitCount()
    verwenden.

    Probieren Sie für den Aufruf von spiel1() verschiedene Werte für
    tiefe aus, und wählen Sie einen, bei dem der Computer im
    Normalfall zwischen 0.1s und 1s zur Auswahl des besten Zugs
    braucht.

    Zusatzfragen:

    1) Ist spiel1() bei gleicher Tiefe durch diese Änderung schneller
    oder langsamer geworden? Um wieviel?
	- Unter die Zeit von statischer Methoden!
	
	-ohne Minimaltiefe
		Tiefe=1 Dauer=0 Millis
		Tiefe=2 Dauer=1 Millis
		Tiefe=3 Dauer=1 Millis
		Tiefe=4 Dauer=2 Millis
		Tiefe=5 Dauer=2 Millis
		Tiefe=6 Dauer=5 Millis
		Tiefe=7 Dauer=20 Millis
		Tiefe=8 Dauer=47 Millis
		Tiefe=9 Dauer=124 Millis
		Tiefe=10 Dauer=324 Millis
		Tiefe=11 Dauer=964 Millis
		Tiefe=12 Dauer=3673 Millis
		Danach >30 sec
	
    - Long:
        Tiefe=0 Dauer=1 Millis
        Tiefe=1 Dauer=0 Millis
        Tiefe=2 Dauer=0 Millis
        Tiefe=3 Dauer=2 Millis
        Tiefe=4 Dauer=1 Millis
        Tiefe=5 Dauer=4 Millis
        Tiefe=6 Dauer=12 Millis
        Tiefe=7 Dauer=58 Millis
        Tiefe=8 Dauer=374 Millis
        Tiefe=9 Dauer=1869 Millis
        Tiefe=10 Dauer=12302 Millis

     - Klassen:
        Tiefe=0 Dauer=2 Millis
        Tiefe=1 Dauer=1 Millis
        Tiefe=2 Dauer=11 Millis
        Tiefe=3 Dauer=5 Millis
        Tiefe=4 Dauer=48 Millis
        Tiefe=5 Dauer=84 Millis
        Tiefe=6 Dauer=424 Millis
        Tiefe=7 Dauer=3021 Millis
        Tiefe=8 Dauer=19777 Millis

     - Statische Methoden:
        Tiefe=0 Dauer=2 Millis
        Tiefe=1 Dauer=2 Millis
        Tiefe=2 Dauer=10 Millis
        Tiefe=3 Dauer=6 Millis
        Tiefe=4 Dauer=10 Millis
        Tiefe=5 Dauer=54 Millis
        Tiefe=6 Dauer=301 Millis
        Tiefe=7 Dauer=1954 Millis
        Tiefe=8 Dauer=14151 Millis

    2) Welche Methoden mussten Sie für diese Änderung ändern und
    welche sind gleichgeblieben?  Hätten Sie einige der geänderten
    Methoden anders schreiben können, um die Notwendigkeit von
    Änderungen zu vermeiden?  Gibt es neben den Kosten der Veränderung
    bei manchen Methoden auch Vorteile durch die Änderung?  Welche?
    - siehe Methodenkommentare
*/
class Spielfeld {
    private long spieler1;
    private long spieler2;

    public static final int breite = 7;
    public static final int hoehe  = 6;

    public static long getBit(long ID ,int position)
    {
        return (ID >> position) & 1;
    }

    public Spielfeld() {
        spieler1=0;
        spieler2=0;
    }

    private Spielfeld(long spieler1, long spieler2) { //musste angepasst werden da es array nicht mehr gibt. vermeidbar, wenn zug rückgängig statt spielfeld klonen. keine Vorteile
        this.spieler1=spieler1;
        this.spieler2=spieler2;
    }

    public void spielstand(){
        for(int a = hoehe-1; a >= 0; a--) {
            System.out.print('|');
            for (int b = 0; b < breite; b++) {
                switch(feld(a,b)){
                    case 0:
                        System.out.print(' ');
                        break;
                    case 1:
                        System.out.print('x');
                        break;
                    case 2:
                        System.out.print('o');
                        break;
                }
            }
            System.out.print('|'+System.lineSeparator());
        }
        System.out.print('+');
        for(int a = 0; breite > a; a++)
            System.out.print('-');
        System.out.print('+'+System.lineSeparator());
    }

    public int feld(int reihe, int spalte) { //musste geändert werden wegen Zugriff auf feld. unvermeidbar. Keine unmittelbare Vorteile
        return (int) (getBit(spieler1,reihe*8+spalte)*1+getBit(spieler2,reihe*8+spalte)*2);
    }

    public Spielfeld copy() { //musste geändert werden da nicht mehr array übergeben wird. vermeidbar, wenn zug rückgängig statt spielfeld klonen. Schnellere ausführung da Array nicht kopiert werden muss.
        return new Spielfeld(spieler1,spieler2);
    }

    public Spielfeld zug(int spieler, int spalte) { //musste geändert werden da in array geschrieben wurde. unvermeidbar. kein vorteil
        if(spalte>=breite) return null;
        for (int a = 0; a < hoehe; a++) {
            if (feld(a,spalte) == 0) {
                if(spieler==1) {
                    spieler1 = spieler1 | 1L << a*8+spalte;
                } else {
                    spieler2 = spieler2 | 1L << a*8+spalte;
                }
                return this;
            }
        }
        return null;
    }

    public boolean sieg(int spieler) { //musste geändert werden. Hilfsmethode fiel weg. unvermeidbar. schnellere ausführung
        long x;
        if(spieler == 1) {
           x = spieler1;
        } else {
            x = spieler2;
        }

        return ((x>>(0*9)) & (x>>(1*9)) & (x>>(2*9)) & (x>>(3*9)))>0 ||
             ((x>>(0*1)) & (x>>(1*1)) & (x>>(2*1)) & (x>>(3*1)))>0 ||
             ((x>>(0*7)) & (x>>(1*7)) & (x>>(2*7)) & (x>>(3*7)))>0 ||
             ((x>>(0*8)) & (x>>(1*8)) & (x>>(2*8)) & (x>>(3*8)))>0;
    }

    public int wert1(int spieler){ //musste geändert werden. Hilfsmethode fiel weg. unvermeidbar. schnellere ausführung
        int z;
        int d;
        int v;
        long x;
        if(spieler == 1) {
            x = spieler1;
        } else {
            x = spieler2;
        }

        v = Long.bitCount(((x>>(0*9)) & (x>>(1*9)) & (x>>(2*9)) & (x>>(3*9))))+
                Long.bitCount(((x>>(0*1)) & (x>>(1*1)) & (x>>(2*1)) & (x>>(3*1)))) +
                Long.bitCount(((x>>(0*7)) & (x>>(1*7)) & (x>>(2*7)) & (x>>(3*7)))) +
                Long.bitCount(((x>>(0*8)) & (x>>(1*8)) & (x>>(2*8)) & (x>>(3*8))));
        d = Long.bitCount(((x>>(0*9)) & (x>>(1*9)) & (x>>(2*9))))+
                Long.bitCount(((x>>(0*1)) & (x>>(1*1)) & (x>>(2*1)))) +
                Long.bitCount(((x>>(0*7)) & (x>>(1*7)) & (x>>(2*7)))) +
                Long.bitCount(((x>>(0*8)) & (x>>(1*8)) & (x>>(2*8))));
        z = Long.bitCount(((x>>(0*9)) & (x>>(1*9))))+
                Long.bitCount(((x>>(0*1)) & (x>>(1*1)))) +
                Long.bitCount(((x>>(0*7)) & (x>>(1*7)))) +
                Long.bitCount(((x>>(0*8)) & (x>>(1*8))));


        return 1*z+100*d+10000*v;
    }

    public int wert(int spieler){
        switch (spieler) {
            case 1:
                return wert1(1)-wert1(2);
            case 2:
                return wert1(2)-wert1(1);
        }
        return -1;
    }

    public int negamax(int spieler, int tiefe){
        if(tiefe < 1)
            return wert(spieler);
        Spielfeld sim = this.copy();
        int max = Integer.MIN_VALUE;
        int nextSpieler = (spieler==1)?2:1;
        for(int s=0, wert;s<=sim.breite;s++) {
            if(sim.zug(spieler,s)!=null) {
                if(sim.sieg(spieler))
                    wert = Integer.MAX_VALUE;
                else
                    wert=-sim.negamax(nextSpieler,tiefe-1);
                if(max<wert)
                    max=wert;
            }
            sim=this.copy();
        }
        return max;
    }

    public int bester(int spieler, int tiefe){
        Spielfeld sim = this.copy();
        int spalte = -1;
        int max = Integer.MIN_VALUE;

        for(int s=0, wert;s<sim.breite;s++) {
            if(sim.zug(spieler,s)!=null) {
                wert=sim.negamax(spieler,tiefe-1);
                if(max<wert) {
                    max=wert;
                    spalte=s;
                }
            }
            sim=this.copy();
        }

        return spalte;
    }
}

class Viergewinnt {
    public static void spiel1(int tiefe){
        Spielfeld f = new Spielfeld();
        int runde = 0;
        int cpu = -1;
        do {
            System.out.print("Spieler beginnt? [Y|N]");
            String s = System.console().readLine();
            if (s.toUpperCase().equals("Y"))
                cpu = 2;
            else if (s.toUpperCase().equals("N"))
                cpu = 1;
            System.out.flush();
        } while (cpu==-1);
        do {
            System.out.flush();
            System.out.println("Runde "+ runde);
            f.spielstand();
            System.out.println("Spieler "+(runde%2 + 1)+" ist am Zug");
            String s = "";
            if((runde%2 + 1)!=cpu) {
                System.out.print("Bitte Reihe angeben [0-" + (f.breite - 1) + "]: ");
                s = System.console().readLine();
            } else {
                s = ""+f.bester(cpu,tiefe);
            }
            try {
                if (f.zug((runde % 2 + 1), Integer.parseInt(s)) == null)
                    continue;
            } catch (Exception ex) {
                continue;
            }
            if(f.sieg((runde%2 + 1))) {
                System.out.flush();
                f.spielstand();
                System.out.println("Spieler "+ (runde%2 + 1) + " gewinnt!");
                break;
            }
            runde++;
        } while(runde <= (f.hoehe * f.breite));
        if(runde > (f.hoehe * f.breite)) {
            System.out.flush();
            f.spielstand();
            System.out.println("Unentschieden");
        }
        System.out.println("Beliebige Taste zum Beenden drücken.");
        String s = System.console().readLine();
    }

    public static void spiel(){
        Spielfeld f = new Spielfeld();
        int runde = 0;
        do {
            System.out.flush();
            System.out.println("Runde "+ runde);
            f.spielstand();
            System.out.println("Spieler "+(runde%2 + 1)+" ist am Zug");
            System.out.print("Bitte Reihe angeben [0-"+(f.breite-1)+"]: ");
            String s = System.console().readLine();
            try{
                if(f.zug((runde%2 + 1),Integer.parseInt(s)) == null)
                    continue;
            } catch (Exception ex) {
                continue;
            }
            if(f.sieg((runde%2 + 1))) {
                System.out.flush();
                f.spielstand();
                System.out.println("Spieler "+ (runde%2 + 1) + " gewinnt!");
                break;
            }
            runde++;
        } while(runde <= (f.hoehe * f.breite));
        if(runde > (f.hoehe * f.breite)) {
            System.out.flush();
            f.spielstand();
            System.out.println("Unentschieden");
        }
        System.out.println("Beliebige Taste zum Beenden drücken.");
        String s = System.console().readLine();
    }
}

public class Aufgabe3 {
    //> java -cp D:\Documents\TUW-PK2016W\Aufgabenblatt7\Aufgabe3\build\classes\main Aufgabe3
    public static void main(String[] args) {

        Viergewinnt.spiel1(8);

        //Viergewinnt.spiel();

        // Test Runtime:
        /*Spielfeld f = new Spielfeld();
        f = f.zug(2,2);
        f = f.zug(1,3);
        f = f.zug(2,4);
        f = f.zug(1,5);
        f = f.zug(1,6);
        f = f.zug(1,2);
        f = f.zug(2,3);
        f = f.zug(2,2);

        for(int i = 0; i<=15;i++) {
            long start = System.currentTimeMillis();
            f.bester(1,i);
            long end = System.currentTimeMillis();
            System.out.println("Tiefe="+i+" Dauer="+(end-start)+" Millis");
        }*/
    }
}


