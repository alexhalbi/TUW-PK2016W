class IntTree {
    
    private class Node {
        
        int elem;
        Node left = null;
        Node right = null;
        
        Node(int elem) {
            this.elem = elem;
        }
        
        void add(int elem) {
            if (elem < this.elem) {
                if (this.left == null) {
                    this.left = new Node(elem);
                } else {
                    this.left.add(elem);
                }
            } else {
                if (this.right == null) {
                    this.right = new Node(elem);
                } else {
                    this.right.add(elem);
                }
            }
        }
        
        int countNodes() {
            int cnt = 1;
            if(left != null) {
                cnt+=left.countNodes();
            }
            if(right != null) {
                cnt+=right.countNodes();
            }
            return cnt;
        }
        
        int countLeaves() {
            if(isLeaf()) return 1;
            return left.countLeaves()+right.countLeaves();
        }
        
        int height() {
            if(left==null) {
                if(right==null)
                    return 1;
                return right.height()+1;
            }
            if(right==null)
                return left.height()+1;
            return Math.max(left.height(),right.height())+1; //diese Anweisung ändern oder löschen.
        }
        
        void printLeaves() {
            if(left==null) {
                if(right==null)
                    System.out.println(this.elem);
                else
                    right.printLeaves();
            } else if(right==null)
                left.printLeaves();
            else {
                left.printLeaves();
                if(left.isLeaf() && right.isLeaf()) System.out.println();
                right.printLeaves();
            }
        }
        
        void printInOrderUp() {
            if(left!=null)
                left.printInOrderUp();
            System.out.println(this.elem);
            if(right!=null)
                right.printInOrderUp();
        }
        
        void printInOrderUpSub(int elem) {
            if(elem==this.elem)
                this.printInOrderUp();
            else if(elem < this.elem)
                left.printInOrderUpSub(elem);
            else
                right.printInOrderUpSub(elem);
        }
    
        void printPostOrder() {
            if(left!=null)
                left.printPostOrder();
            if(right!=null)
                right.printPostOrder();
            System.out.println(this.elem);
        }
    
        void printPreOrder() {
            System.out.println(this.elem);
            if(left!=null)
                left.printPreOrder();
            if(right!=null)
                right.printPreOrder();
        }

        boolean isLeaf() {
            if(left==null && right==null) return true;
            return false;
        }
    }
    
    private Node root = null;
    
    public void add(int elem) {
        if (empty()) {
            this.root = new Node(elem);
        } else {
            this.root.add(elem);
        }
    }
    
    public boolean empty() {
        return this.root == null;
    }
    
    public int countNodes() {
        if(root==null) return 0;
        return root.countNodes();
    }
    
    public int countLeaves() {
        if(root==null) return 0;
        return root.countLeaves();
    }
    
    public int height() {
        if(root==null) return 0;
        return root.height();
    }
    
    public void printLeaves() {
        if(root!=null) root.printLeaves();
    }
    
    public void printInOrderUp() {
        if(root!=null) root.printInOrderUp();
    }
    
    public void printInOrderUpSub(int elem) {
       if(root!=null) root.printInOrderUpSub(elem);
    }
    
    public void printPostOrder(){
        if(root!=null) root.printPostOrder();
    }
    
    public void printPreOrder(){
        if(root!=null) root.printPreOrder();
    }
}
