// TODO: Implementieren Sie hier die Klasse Koralle.
public class Koralle implements Meerestier {
    String bezeichnung;
    int regal;

    public Koralle(String bezeichnung,int regal) {
        this.bezeichnung=bezeichnung;
        einordnen(regal);
    }

    @Override
    public void einordnen(int regal) {
        this.regal = regal;
    }

    @Override
    public void ausraeumen() {
        regal = 0;
    }

    @Override
    public void abwiegen(int gewicht) {

    }

    @Override
    public String toString() {
        return bezeichnung+", Regal "+regal+", Gewicht 1000g";
    }
}