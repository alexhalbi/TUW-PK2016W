import java.util.ArrayList;
import java.util.List;

public class ArchivImpl implements Archiv {
    private List<Meerestier> tiere;

    public ArchivImpl() {
        this.tiere = new ArrayList<>();
    }

    @Override
    public void registrieren(Meerestier m) {
        tiere.add(m);
    }

    @Override
    public void ausraeumen() {
        for (Meerestier m:tiere) {
            m.ausraeumen();
        }
    }

    @Override
    public void einordnen() {
        for(int i = 0; i<tiere.size();i++) {
            tiere.get(i).einordnen(i+1);
        }
    }

    @Override
    public void neuWiegen(int[] gewicht) {
        for(int i = 0; i<tiere.size();i++) {
            tiere.get(i).abwiegen(gewicht[i]);
        }
    }

    @Override
    public String toString() {
        if(tiere.size()==0) return "Leeres Archiv";
        String s = "";
        for (Meerestier m:tiere) {
            s += m+System.lineSeparator();
        }
        return s;
    }
}

