public class Fisch implements Meerestier {
    String bezeichnung;
    int regal;
    int gewicht;

    public Fisch(String bezeichnung,int regal,int gewicht) {
        this.bezeichnung=bezeichnung;
        einordnen(regal);
        abwiegen(gewicht);
    }

    @Override
    public void einordnen(int regal) {
        this.regal = regal+200;
    }

    @Override
    public void ausraeumen() {
        regal = 0;
    }

    @Override
    public void abwiegen(int gewicht) {
        this.gewicht=gewicht;
    }

    @Override
    public String toString() {
        return bezeichnung+", Regal "+regal+", Gewicht "+gewicht+"kg";
    }
}