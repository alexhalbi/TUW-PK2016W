public class Schildkroete implements Meerestier {
    String bezeichnung;
    int regal;
    int gewicht;

    public Schildkroete(String bezeichnung,int regal,int gewicht) {
        this.bezeichnung=bezeichnung;
        einordnen(regal);
        this.gewicht=gewicht;
    }

    @Override
    public void einordnen(int regal) {
        this.regal = regal;
    }

    @Override
    public void ausraeumen() {
        regal = 0;
    }

    @Override
    public void abwiegen(int gewicht) {
        this.gewicht = (int) (gewicht*0.9);
    }

    @Override
    public String toString() {
        return bezeichnung+", Regal "+regal+", Gewichtsklasse "+gewicht+"kg";
    }
}