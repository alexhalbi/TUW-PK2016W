public class Saeugetier implements Meerestier {
    String bezeichnung;
    int regal;
    int gewicht;

    public Saeugetier(String bezeichnung,int regal,int gewicht) {
        this.bezeichnung=bezeichnung;
        einordnen(regal);
        abwiegen(gewicht);
    }

    @Override
    public void einordnen(int regal) {
        this.regal = regal+100;
    }

    @Override
    public void ausraeumen() {
    }

    @Override
    public void abwiegen(int gewicht) {
        this.gewicht=gewicht*1000;
    }

    @Override
    public String toString() {
        return bezeichnung+", SpezialRegal "+regal+", Gewicht "+gewicht+"kg";
    }
}